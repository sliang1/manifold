//
//  ModelBurst.cpp
//  latentfuse
//
//  Created by Shangsong Liang on 12/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "ModelBurst.h"

lss::ModelBurst::ModelBurst(lss::Runs runs, lss::Burst bur, lss::CombSUMMNZ combSUMMNZ)
{
    numOfRuns_=runs.getNumOfRuns();
    int weiTyp=1;
    int maxIteTim=0;
    double numOfAsp=0.0;
    double lamOne=0.0;
    double lamTwo=0.0;
    double leaRat=0.0;
    double docNumInRun=0.0;
    double alpha=0.0;
    double beta=0.0;
    double bWeight=0.0;
    lemur::api::ParamGet("weightingType", weiTyp, 1);
    weiTyp_=weiTyp;
    lemur::api::ParamGet("numOfAsp", numOfAsp, 30);
    numOfAsp_=numOfAsp;
    lemur::api::ParamGet("lamOne", lamOne, 0.00001);
    lamOne_=lamOne;
    lemur::api::ParamGet("lamTwo", lamTwo, 0.00001);
    lamTwo_=lamTwo;
    lemur::api::ParamGet("learningRate", leaRat, 0.03);
    leaRat_=leaRat;
    lemur::api::ParamGet("maxIterationTime", maxIteTim, 1000);
    maxIteTim_=maxIteTim;
    lemur::api::ParamGet("docNumInRun", docNumInRun, 30);
    docNumInRun_=docNumInRun;
    lemur::api::ParamGet("alpha2", alpha, 1);
    alpha_=alpha;
    lemur::api::ParamGet("beta", beta, 1);
    beta_=beta;
    lemur::api::ParamGet("bWeight", bWeight, 0.0);
    bWeight_=bWeight;
    qTDSSUM_=combSUMMNZ.getQTDSSUM();
    qTDSMNZ_=combSUMMNZ.getQTDSMNZ();
    this->iniQAllDocInd(runs);
    //this->computQueDocScoBurst(runs, bur);
    //this->computQueDocScoBurst2(runs, bur);
    //this->computQueDocScoBurst3(runs, bur);
    this->start_=std::clock();
    this->computQueDocScoBurst5(runs, bur);
    this->wriEvaResult();
    this->finish_=std::clock();
    //std::cout<<"The execution for TimeRA is: "<<std::endl;
    //std::cout<<(this->finish_-this->start_)/double(CLOCKS_PER_SEC)*1000<<"ms."<<std::endl;
}

void
lss::ModelBurst::iniQAllDocInd(lss::Runs& runs)
{
    const int numOfRuns=runs.getNumOfRuns();
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itDS;
    lss::DOCSCO_T::iterator itAllDocInd;
    double index=0.0;
    
    for(int i=0; i<numOfRuns; i++)
    {
        for(itQDS=runs.qDS[i].begin(); itQDS!=runs.qDS[i].end(); itQDS++)
        {
            itQAllDocInd=qAllDocInd_.find(itQDS->first);
            if(itQAllDocInd==qAllDocInd_.end())
            {
                lss::DOCSCO_T docInd;
                for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
                {
                    docInd.insert(lss::DOCSCO_PAIR_T(itDS->first, 0.0));
                }
                qAllDocInd_.insert(lss::QUEDOCSCO_PAIR_T(itQDS->first, docInd));
            }
            else
            {
                for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
                {
                    itAllDocInd=itQAllDocInd->second.find(itDS->first);
                    if(itAllDocInd==itQAllDocInd->second.end())
                    {
                        itQAllDocInd->second.insert(lss::DOCSCO_PAIR_T(itDS->first, 0.0));
                    }
                }
            }
        }//for(itQDS=runs.qDS[i].begin(); 
    }//for(int i=0; i<numOfRuns;
    for(itQDS=qAllDocInd_.begin(); itQDS!=qAllDocInd_.end(); itQDS++)
    {
        index=0.0;
        for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
        {
            itDS->second=index;
            index=index+1;
        }
    }
}

void
lss::ModelBurst::computQueDocScoBurst(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::DOCDOCIND_T::iterator itDocDocInd;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco; //query-doc-doc-dist
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    lss::DOCSCO_T::iterator itDocSco;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    const double beta=beta_;
    double cost=0.0;
    double sv=0.0;
    //double Iij=0.0;
    double sumn[numOfAsp];
    double summ[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    int k_ind=0;
    bool hasRead=false;
    
    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCDOCIND_T docDocInd;
        lss::DOCSCO_T docSco;
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        itQDocDocSco=bur.qDocDocSco.find(itQAllDocInd->first);
        if(itQDocDocSco==bur.qDocDocSco.end())
        {
            std::cout<<"There is an erro in qDocDocSco in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            docDocInd.insert(lss::DOCDOCIND_PAIR_T(itAllDocInd->first, index));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.5;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.5;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-sv, 2.0);
                }
            }
            //std::cout<<"error="<<cost<<std::endl;
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn[a]=sumn[a]+Iij[i][j]*(sv-r[i][j])*v[j][a];
                    }
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    s[a][i]=s[a][i]-leaRat*sumn[a]*s[a][i];
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+Iij[i][j]*(sv-r[i][j])*s[a][i];
                    }
                }
                itDocIndDoc=docIndDoc.find(j);
                itDocTimInd=bur.docTimInd.find(itDocIndDoc->second);
                itTimIndDocVec=itQTimIndDocVec->second.find(itDocTimInd->second);
                
                itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                {
                    itDocDocInd=docDocInd.find(itDocSco->first);
                    k_ind=itDocDocInd->second;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+beta*itDocSco->second*(v[j][a]-v[k_ind][a])*v[k_ind][a];
                        std::cout<<itDocSco->second<<std::endl;
                    }
                }
                /*
                if(itTimIndDocVec->second.size()>1)
                {
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+alpha*v[j][a];
                        for(int k=0; k<itTimIndDocVec->second.size(); k++)
                        {
                            summ[a]=summ[a]-alpha*1.0/itTimIndDocVec->second.size()*v[k][a];
                        }
                    }
                }
                 */
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                    v[j][a]=v[j][a]-leaRat*summ[a]*v[j][a];
                }
            } //for(int j=0; j<numOfDoc; j++)
        }//for(int ite=0; ite<maxIteTim_; ite++)
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    svLis=svLis+Iij[i][j]*s[a][i]*v[j][a];
                }
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLis));
        }
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
    
    /*
     for(itQDS=runs.qDS[1].begin(); itQDS!=runs.qDS[1].end(); itQDS++)
     {
     //std::cout<<" query="<<itQDS->first<<std::endl;
     if(itQDS->first=="MB001")
     {
     for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
     {
     std::cout<<"*"<<itDS->first<<std::endl;
     }
     }
     count=count+itQDS->second.size();
     }
     */
}

void
lss::ModelBurst::computQueDocScoBurst2(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::DOCDOCIND_T::iterator itDocDocInd;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco; //query-doc-doc-dist
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    lss::DOCSCO_T::iterator itDocSco;
    lss::QUEDOCSCO_T::iterator itQTDSSUM;
    lss::DOCSCO_T::iterator itTDSSUM;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    const double beta=beta_;
    double cost=0.0;
    double sv=0.0;
    double isv=0.0; //sum I S V
    double ir=0.0; // sum I R
    //double Iij=0.0;
    double sumn[numOfAsp];
    double summ[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    int k_ind=0;
    bool hasRead=false;
    
    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCDOCIND_T docDocInd;
        lss::DOCSCO_T docSco;
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        itQDocDocSco=bur.qDocDocSco.find(itQAllDocInd->first);
        if(itQDocDocSco==bur.qDocDocSco.end())
        {
            std::cout<<"There is an erro in qDocDocSco in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        itQTDSSUM=qTDSSUM_.find(itQAllDocInd->first);
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            docDocInd.insert(lss::DOCDOCIND_PAIR_T(itAllDocInd->first, index));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.5;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.5;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-sv, 2.0);
                }
            }
            std::cout<<"error="<<cost<<std::endl;
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn[a]=sumn[a]+Iij[i][j]*(sv-r[i][j])*v[j][a];
                    }
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        itTDSSUM=itQTDSSUM->second.find(itDocSco->first);
                        if(itTDSSUM==itQTDSSUM->second.end())
                        {
                            continue;
                        }
                        k_ind=itDocDocInd->second;
                        isv=0.0;
                        ir=0.0;
                        for(int i2=0; i2<numOfLis; i2++)
                        {
                            for(int a=0; a<numOfAsp; a++)
                            {
                                if(Iij[i2][j]>0)
                                {
                                    isv=isv+s[a][i2]*v[j][a];
                                }
                            }
                            if(Iij[i2][k_ind]>0)
                            {
                                ir=ir+r[i2][k_ind];
                            }
                        }//for(int i2=0; i2<numOfLis; i2++)
                        for(int a=0; a<numOfAsp; a++)
                        {
                            if(Iij[i][j]>0)
                            {
                                sumn[a]=sumn[a]+beta*(numOfLis*1.0/itDocDocSco->second.size())*itDocSco->second*(isv-ir)*v[j][a];
                            }
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                }//for(int j=0; j<numOfDoc; j++)
                
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    s[a][i]=s[a][i]-leaRat*sumn[a]*s[a][i];
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+Iij[i][j]*(sv-r[i][j])*s[a][i];
                    }
                }
                itDocIndDoc=docIndDoc.find(j);
                itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                {
                    itDocDocInd=docDocInd.find(itDocSco->first);
                    itTDSSUM=itQTDSSUM->second.find(itDocSco->first);
                    if(itTDSSUM==itQTDSSUM->second.end())
                    {
                        continue;
                    }
                    k_ind=itDocDocInd->second;
                    isv=0.0;
                    ir=0.0;
                    for(int i=0; i<numOfLis; i++)
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            if(Iij[i][j]>0)
                            {
                                isv=isv+s[a][i]*v[j][a];
                            }
                        }
                        if(Iij[i][k_ind]>0)
                        {
                            ir=ir+r[i][k_ind];
                        }
                    }
                    
                    for(int i=0; i<numOfLis; i++)
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            if(Iij[i][j]>0)
                            {
                                summ[a]=summ[a]+beta*(numOfLis*1.0/itDocDocSco->second.size())*itDocSco->second*(isv-ir)*s[a][i];
                            }
                        }
                    }//for(int i=0; i<numOfLis; i++)
                }
                /*
                 if(itTimIndDocVec->second.size()>1)
                 {
                 for(int a=0; a<numOfAsp; a++)
                 {
                 summ[a]=summ[a]+alpha*v[j][a];
                 for(int k=0; k<itTimIndDocVec->second.size(); k++)
                 {
                 summ[a]=summ[a]-alpha*1.0/itTimIndDocVec->second.size()*v[k][a];
                 }
                 }
                 }
                 */
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                    v[j][a]=v[j][a]-leaRat*summ[a]*v[j][a];
                }
            } //for(int j=0; j<numOfDoc; j++)
        }//for(int ite=0; ite<maxIteTim_; ite++)
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    if(Iij[i][j]>0)
                    {
                        svLis=svLis+s[a][i]*v[j][a];
                    }
                }
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLis));
        }
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
}

void
lss::ModelBurst::computQueDocScoBurst3(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::DOCSCO_T::iterator itDSj; //itDS for j-th doc
    lss::DOCSCO_T::iterator itDSk; //itDS for k-th doc
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::DOCDOCIND_T::iterator itDocDocInd;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco; //query-doc-doc-dist
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    lss::DOCSCO_T::iterator itDocSco;
    lss::QUEDOCSCO_T::iterator itQTDSSUM;
    lss::DOCSCO_T::iterator itTDSSUM;
    lss::INDCOV_T indCovTemp;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    const double beta=beta_;
    const double bWeight=bWeight_;
    double cost=0.0;
    double cost2=0.0;
    double sv=0.0;
    double isv=0.0; //sum I S V
    double ir=0.0; // sum I R
    //double Iij=0.0;
    double sumn[numOfAsp];
    double sumn2[numOfAsp];
    double summ[numOfAsp];
    double summ2[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    int k_ind=0;
    int k=0;
    double countK=0.0;
    bool hasRead=false;
    double max=0.0;
    double maxTemp=0.0;
    int maxIndTemp=0;
    
    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCDOCIND_T docDocInd;
        lss::DOCSCO_T docSco;
        std::vector<lss::INDCOV_T> indCovVec; //index-covirance vector
        std::vector<lss::INDCOV_T>::iterator itIndCovVec;
        std::vector<lss::INDCOV_T>::iterator itIndCovVec2;
        std::vector<int> indVec; //final ranking document index vector
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        double e[numOfDoc]; //expectation
        double sigma[numOfDoc]; //variance
        double rho[numOfDoc][numOfDoc]; //covariance
        double rho_sv;
        double rho_sv2;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        itQDocDocSco=bur.qDocDocSco.find(itQAllDocInd->first);
        if(itQDocDocSco==bur.qDocDocSco.end())
        {
            std::cout<<"There is an erro in qDocDocSco in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        itQTDSSUM=qTDSSUM_.find(itQAllDocInd->first);
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            docDocInd.insert(lss::DOCDOCIND_PAIR_T(itAllDocInd->first, index));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.00005;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.00005;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-sv, 2.0);
                }
            }
            //std::cout<<"error="<<cost<<std::endl;
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn[a]=sumn[a]+Iij[i][j]*(sv-r[i][j])*v[j][a];
                    }
                }
                
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn2[a]=0.0;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    itQDS=runs.qDS[i].find(itQAllDocInd->first);
                    itDSj=itQDS->second.find(itDocIndDoc->second);
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDSj->first<<" "<<itDSj->second<<" "<<itDSk->first<<" "<<itDSk->second<<std::endl;
                            continue;
                        }
                        countK=countK+1.0;
                        cost=cost+itDocSco->second*Iij[i][k]*pow((r[i][k]-sv),2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn2[a]=sumn2[a]+itDocSco->second*Iij[i][k]*(sv-r[i][k])*v[j][a];
                            //std::cout<<sv-r[i][k]<<" sv="<<sv<<" r="<<r[i][k]<<std::endl;
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn[a]=sumn[a]+beta*Iij[i][j]*(sv-r[i][j])*v[j][a];
                        }
                        continue;
                    }
                    else
                    {
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        cost=cost+Iij[i][j]*pow((r[i][j]-sv), 2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn[a]=sumn[a]+beta/countK*sumn2[a];
                            //std::cout<<sumn[a]<<"  "<<beta/countK*sumn2[a]<<std::endl;
                        }
                    }
                }//for(int j=0; j<numOfDoc; j++)
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    s[a][i]=s[a][i]-leaRat*sumn[a]*s[a][i];
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            if(cost<cost2)
            {
                //std::cout<<"cost="<<cost<<std::endl;
            }
            cost2=cost;
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+Iij[i][j]*(sv-r[i][j])*s[a][i];
                    }
                }
                
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ2[a]=0.0;
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itQDS=runs.qDS[i].find(itQAllDocInd->first);
                        itDSj=itQDS->second.find(itDocIndDoc->second);
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            continue;
                        }
                        countK=countK+1.0;
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ2[a]=summ2[a]+itDocSco->second*Iij[i][k]*(sv-r[i][k])*s[a][i];
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ[a]=summ[a]+beta*Iij[i][j]*(sv-r[i][j])*s[a][i];
                        }
                        continue;
                    }
                    else
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ[a]=summ[a]+beta/countK*summ2[a];
                        }
                    }
                }//for(int i=0; i<numOfLis; i++)
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    v[j][a]=v[j][a]-leaRat*summ[a]*v[j][a];
                }
            } //for(int j=0; j<numOfDoc; j++)
        }//for(int ite=0; ite<maxIteTim_; ite++)
        /////////////////////////
        /*
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                if(Iij[i][j]>0)
                {
                    for(int a=0; a<numOfAsp; a++)
                    {
                        svLis=svLis+s[a][i]*v[j][a];
                    }
                }
            }//for(int i=0; i<numOfLis; i++)
            e[j]=svLis;
            sigma[j]=0.0; //initialize sigma
            for(int i=0; i<numOfLis; i++)
            {
                if(Iij[i][j]<=0)
                {
                    sigma[j]=sigma[j]+1.0/numOfLis*pow(0.0-e[j]/numOfLis, 2.0);
                }
                else
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    sigma[j]=sigma[j]+1.0/numOfLis*pow(sv-e[j]/numOfLis, 2.0);
                }
            }
            //std::cout<<"expactation="<<e[j]<<"  sigma="<<sigma[j]<<std::endl;
        }//for(int j=0; j<numOfDoc; j++)
        for(int j=0; j<numOfDoc; j++)
        {
            for(int j2=0; j2<numOfDoc; j2++)
            {
                rho[j][j2]=0.0; //initialize rho
                for(int i=0; i<numOfLis; i++)
                {
                    rho_sv=0.0;
                    rho_sv2=0.0;
                    if(Iij[i][j]<=0.0)
                    {
                        rho_sv=0.0;
                    }
                    else
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            rho_sv=rho_sv+s[a][i]*v[j][a];
                        }
                    }
                    if(Iij[i][j2]<=0.0)
                    {
                        rho_sv2=0.0;
                    }
                    else
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            rho_sv2=rho_sv2+s[a][i]*v[j2][a];
                        }
                    }
                    rho[j][j2]=rho[j][j2]+1.0/numOfLis*(rho_sv-1.0/numOfLis*e[j])*(rho_sv2-1.0/numOfLis*e[j2]);
                }
                //std::cout<<"expactation="<<e[j]<<"  coviariance="<<rho[j][j2]<<std::endl;
            }//for(int j2=0; j2<numOfDoc; j2++)
        }//for(int j=0; j<numOfDoc; j++)
        for(int j=0; j<numOfDoc; j++)
        {
            indCovTemp.ind=j;
            indCovTemp.cov=0.0; //sum_{i=1}^{k-1}
            indCovVec.push_back(indCovTemp);
        }
        for(int j=0; j<numOfDoc; j++)
        {
            max=0.0;
            for(itIndCovVec=indCovVec.begin(); itIndCovVec!=indCovVec.end(); itIndCovVec++)
            {
                maxTemp=e[itIndCovVec->ind]-bWeight*1.0/log2(j+1.0+1.0)*sigma[itIndCovVec->ind]-2.0*bWeight*1.0/log2(j+1.0+1.0)*itIndCovVec->cov;
                if(maxTemp>max)
                {
                    itIndCovVec2=itIndCovVec;
                    max=maxTemp;
                }
            }
            indVec.push_back(itIndCovVec2->ind);
            for(int j2=0; j2<indCovVec.size(); j2++)
            {
                indCovVec[j2].cov=indCovVec[j2].cov+rho[itIndCovVec2->ind][j2];
            }
            indCovVec.erase(itIndCovVec2);
        }//for(int j=0; j<numOfDoc; j++)
        
        for(int j=0; j<numOfDoc; j++)
        {
            index=indVec[j];
            itDocIndDoc=docIndDoc.find(index);
            //docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, numOfDoc-j));
        }
         */
        /////////////////
        
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    if(Iij[i][j]>0)
                    {
                        svLis=svLis+s[a][i]*v[j][a];
                    }
                }
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLis));
        }
        
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
}

void
lss::ModelBurst::computQueDocScoBurst33(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::DOCSCO_T::iterator itDSj; //itDS for j-th doc
    lss::DOCSCO_T::iterator itDSk; //itDS for k-th doc
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::DOCDOCIND_T::iterator itDocDocInd;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco; //query-doc-doc-dist
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    lss::DOCSCO_T::iterator itDocSco;
    lss::QUEDOCSCO_T::iterator itQTDSSUM;
    lss::DOCSCO_T::iterator itTDSSUM;
    lss::INDCOV_T indCovTemp;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    const double beta=beta_;
    const double bWeight=bWeight_;
    double cost=0.0;
    double cost2=0.0;
    double sv=0.0;
    double isv=0.0; //sum I S V
    double ir=0.0; // sum I R
    //double Iij=0.0;
    double sumn[numOfAsp];
    double sumn2[numOfAsp];
    double summ[numOfAsp];
    double summ2[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    int k_ind=0;
    int k=0;
    double countK=0.0;
    bool hasRead=false;
    double max=0.0;
    double maxTemp=0.0;
    int maxIndTemp=0;
    
    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCDOCIND_T docDocInd;
        lss::DOCSCO_T docSco;
        std::vector<lss::INDCOV_T> indCovVec; //index-covirance vector
        std::vector<lss::INDCOV_T>::iterator itIndCovVec;
        std::vector<lss::INDCOV_T>::iterator itIndCovVec2;
        std::vector<int> indVec; //final ranking document index vector
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        double e[numOfDoc]; //expectation
        double sigma[numOfDoc]; //variance
        double rho[numOfDoc][numOfDoc]; //covariance
        double rho_sv;
        double rho_sv2;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        itQDocDocSco=bur.qDocDocSco.find(itQAllDocInd->first);
        if(itQDocDocSco==bur.qDocDocSco.end())
        {
            std::cout<<"There is an erro in qDocDocSco in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        itQTDSSUM=qTDSSUM_.find(itQAllDocInd->first);
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            docDocInd.insert(lss::DOCDOCIND_PAIR_T(itAllDocInd->first, index));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.005;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.005;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-sv, 2.0);
                }
            }
            //std::cout<<"error="<<cost<<std::endl;
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn[a]=sumn[a]+Iij[i][j]*(sv-r[i][j])*v[j][a];
                    }
                }
                
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn2[a]=0.0;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    itQDS=runs.qDS[i].find(itQAllDocInd->first);
                    itDSj=itQDS->second.find(itDocIndDoc->second);
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDSj->first<<" "<<itDSj->second<<" "<<itDSk->first<<" "<<itDSk->second<<std::endl;
                            continue;
                        }
                        countK=countK+1.0;
                        cost=cost+itDocSco->second*Iij[i][k]*pow((r[i][k]-sv),2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn2[a]=sumn2[a]+itDocSco->second*Iij[i][k]*(sv-r[i][k])*v[j][a];
                            //std::cout<<sv-r[i][k]<<" sv="<<sv<<" r="<<r[i][k]<<std::endl;
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        continue;
                    }
                    else
                    {
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        cost=cost+Iij[i][j]*pow((r[i][j]-sv), 2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn[a]=sumn[a]+beta/countK*sumn2[a];
                            //std::cout<<sumn[a]<<"  "<<beta/countK*sumn2[a]<<std::endl;
                        }
                    }
                }//for(int j=0; j<numOfDoc; j++)
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    s[a][i]=s[a][i]-leaRat*sumn[a];
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            if(cost<cost2)
            {
                //std::cout<<"cost="<<cost<<std::endl;
            }
            cost2=cost;
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+Iij[i][j]*(sv-r[i][j])*s[a][i];
                    }
                }
                
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ2[a]=0.0;
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itQDS=runs.qDS[i].find(itQAllDocInd->first);
                        itDSj=itQDS->second.find(itDocIndDoc->second);
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            continue;
                        }
                        countK=countK+1.0;
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ2[a]=summ2[a]+itDocSco->second*Iij[i][k]*(sv-r[i][k])*s[a][i];
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        continue;
                    }
                    else
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ[a]=summ[a]+beta/countK*summ2[a];
                        }
                    }
                }//for(int i=0; i<numOfLis; i++)
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    v[j][a]=v[j][a]-leaRat*summ[a];
                }
            } //for(int j=0; j<numOfDoc; j++)
        }//for(int ite=0; ite<maxIteTim_; ite++)
        /////////////////////////
        /*
         for(int j=0; j<numOfDoc; j++)
         {
         svLis=0.0;
         for(int i=0; i<numOfLis; i++)
         {
         if(Iij[i][j]>0)
         {
         for(int a=0; a<numOfAsp; a++)
         {
         svLis=svLis+s[a][i]*v[j][a];
         }
         }
         }//for(int i=0; i<numOfLis; i++)
         e[j]=svLis;
         sigma[j]=0.0; //initialize sigma
         for(int i=0; i<numOfLis; i++)
         {
         if(Iij[i][j]<=0)
         {
         sigma[j]=sigma[j]+1.0/numOfLis*pow(0.0-e[j]/numOfLis, 2.0);
         }
         else
         {
         sv=0.0;
         for(int a=0; a<numOfAsp; a++)
         {
         sv=sv+s[a][i]*v[j][a];
         }
         sigma[j]=sigma[j]+1.0/numOfLis*pow(sv-e[j]/numOfLis, 2.0);
         }
         }
         //std::cout<<"expactation="<<e[j]<<"  sigma="<<sigma[j]<<std::endl;
         }//for(int j=0; j<numOfDoc; j++)
         for(int j=0; j<numOfDoc; j++)
         {
         for(int j2=0; j2<numOfDoc; j2++)
         {
         rho[j][j2]=0.0; //initialize rho
         for(int i=0; i<numOfLis; i++)
         {
         rho_sv=0.0;
         rho_sv2=0.0;
         if(Iij[i][j]<=0.0)
         {
         rho_sv=0.0;
         }
         else
         {
         for(int a=0; a<numOfAsp; a++)
         {
         rho_sv=rho_sv+s[a][i]*v[j][a];
         }
         }
         if(Iij[i][j2]<=0.0)
         {
         rho_sv2=0.0;
         }
         else
         {
         for(int a=0; a<numOfAsp; a++)
         {
         rho_sv2=rho_sv2+s[a][i]*v[j2][a];
         }
         }
         rho[j][j2]=rho[j][j2]+1.0/numOfLis*(rho_sv-1.0/numOfLis*e[j])*(rho_sv2-1.0/numOfLis*e[j2]);
         }
         //std::cout<<"expactation="<<e[j]<<"  coviariance="<<rho[j][j2]<<std::endl;
         }//for(int j2=0; j2<numOfDoc; j2++)
         }//for(int j=0; j<numOfDoc; j++)
         for(int j=0; j<numOfDoc; j++)
         {
         indCovTemp.ind=j;
         indCovTemp.cov=0.0; //sum_{i=1}^{k-1}
         indCovVec.push_back(indCovTemp);
         }
         for(int j=0; j<numOfDoc; j++)
         {
         max=0.0;
         for(itIndCovVec=indCovVec.begin(); itIndCovVec!=indCovVec.end(); itIndCovVec++)
         {
         maxTemp=e[itIndCovVec->ind]-bWeight*1.0/log2(j+1.0+1.0)*sigma[itIndCovVec->ind]-2.0*bWeight*1.0/log2(j+1.0+1.0)*itIndCovVec->cov;
         if(maxTemp>max)
         {
         itIndCovVec2=itIndCovVec;
         max=maxTemp;
         }
         }
         indVec.push_back(itIndCovVec2->ind);
         for(int j2=0; j2<indCovVec.size(); j2++)
         {
         indCovVec[j2].cov=indCovVec[j2].cov+rho[itIndCovVec2->ind][j2];
         }
         indCovVec.erase(itIndCovVec2);
         }//for(int j=0; j<numOfDoc; j++)
         
         for(int j=0; j<numOfDoc; j++)
         {
         index=indVec[j];
         itDocIndDoc=docIndDoc.find(index);
         //docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, numOfDoc-j));
         }
         */
        /////////////////
        
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    if(Iij[i][j]>0)
                    {
                        svLis=svLis+s[a][i]*v[j][a];
                    }
                }
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLis));
        }
        
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
}

void
lss::ModelBurst::computQueDocScoBurst4(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::DOCSCO_T::iterator itDSj; //itDS for j-th doc
    lss::DOCSCO_T::iterator itDSk; //itDS for k-th doc
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::DOCDOCIND_T::iterator itDocDocInd;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco; //query-doc-doc-dist
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    lss::DOCSCO_T::iterator itDocSco;
    lss::QUEDOCSCO_T::iterator itQTDSSUM;
    lss::DOCSCO_T::iterator itTDSSUM;
    lss::INDCOV_T indCovTemp;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    const double beta=beta_;
    const double bWeight=bWeight_;
    double cost=0.0;
    double cost2=0.0;
    double sv=0.0;
    double isv=0.0; //sum I S V
    double ir=0.0; // sum I R
    //double Iij=0.0;
    double sumn[numOfAsp];
    double sumn2[numOfAsp];
    double summ[numOfAsp];
    double summ2[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    int k_ind=0;
    int k=0;
    double countK=0.0;
    bool hasRead=false;
    double max=0.0;
    double maxTemp=0.0;
    int maxIndTemp=0;
    
    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCDOCIND_T docDocInd;
        lss::DOCSCO_T docSco;
        std::vector<lss::INDCOV_T> indCovVec; //index-covirance vector
        std::vector<lss::INDCOV_T>::iterator itIndCovVec;
        std::vector<lss::INDCOV_T>::iterator itIndCovVec2;
        std::vector<int> indVec; //final ranking document index vector
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        double e[numOfDoc]; //expectation
        double sigma[numOfDoc]; //variance
        double rho[numOfDoc][numOfDoc]; //covariance
        double rho_sv;
        double rho_sv2;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        itQDocDocSco=bur.qDocDocSco.find(itQAllDocInd->first);
        if(itQDocDocSco==bur.qDocDocSco.end())
        {
            std::cout<<"There is an erro in qDocDocSco in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        itQTDSSUM=qTDSSUM_.find(itQAllDocInd->first);
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            docDocInd.insert(lss::DOCDOCIND_PAIR_T(itAllDocInd->first, index));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.1;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.1;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-sv, 2.0);
                }
            }
            //std::cout<<"error="<<cost<<std::endl;
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        if(fabs(sv-r[i][j])>SMALLVALUE)
                        {
                            sumn[a]=sumn[a]+Iij[i][j]*(sv-r[i][j])*v[j][a];
                        }
                    }
                }
                
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn2[a]=0.0;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    itQDS=runs.qDS[i].find(itQAllDocInd->first);
                    itDSj=itQDS->second.find(itDocIndDoc->second);
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDSj->first<<" "<<itDSj->second<<" "<<itDSk->first<<" "<<itDSk->second<<std::endl;
                            continue;
                        }
                        countK=countK+1.0;
                        cost=cost+itDocSco->second*Iij[i][k]*pow((r[i][k]-sv),2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            if(fabs(sv-r[i][k])>SMALLVALUE)
                            {
                                sumn2[a]=sumn2[a]+itDocSco->second*Iij[i][k]*(sv-r[i][k])*v[j][a];
                            }
                            //std::cout<<"r="<<r[i][k]<<" gf="<<gF(sv)<<std::endl;
                            //std::cout<<sv-r[i][k]<<" sv="<<sv<<" r="<<r[i][k]<<std::endl;
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        /*for(int a=0; a<numOfAsp; a++)
                         {
                         sumn[a]=sumn[a]+beta*Iij[i][j]*(sv-r[i][j])*v[j][a];
                         }*/
                        continue;
                    }
                    else
                    {
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        cost=cost+Iij[i][j]*pow((r[i][j]-sv), 2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn[a]=sumn[a]+beta/countK*sumn2[a];
                            //std::cout<<sumn[a]<<"  "<<beta/countK*sumn2[a]<<std::endl;
                        }
                    }
                }//for(int j=0; j<numOfDoc; j++)
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    //s[a][i]=s[a][i]-leaRat*sumn[a]*s[a][i];
                    s[a][i]=s[a][i]-leaRat*sumn[a];
                    //std::cout<<summ[a]<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            if(cost<cost2)
            {
                //std::cout<<"cost="<<cost<<std::endl;
            }
            cost2=cost;
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        if(fabs(sv-r[i][j])>SMALLVALUE)
                        {
                            summ[a]=summ[a]+Iij[i][j]*(sv-r[i][j])*s[a][i];
                        }
                        //std::cout<<"sv="<<sv<<" gf="<<gF(sv)<<" gDerF="<<gDerF(sv)<<std::endl;
                    }
                }
                
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ2[a]=0.0;
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itQDS=runs.qDS[i].find(itQAllDocInd->first);
                        itDSj=itQDS->second.find(itDocIndDoc->second);
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            continue;
                        }
                        countK=countK+1.0;
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        for(int a=0; a<numOfAsp; a++)
                        {
                            if(fabs(sv-r[i][k])>SMALLVALUE)
                            {
                                summ2[a]=summ2[a]+itDocSco->second*Iij[i][k]*(sv-r[i][k])*s[a][i];
                            }
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        /*for(int a=0; a<numOfAsp; a++)
                         {
                         summ[a]=summ[a]+beta*Iij[i][j]*(sv-r[i][j])*s[a][i];
                         }*/
                        continue;
                    }
                    else
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ[a]=summ[a]+beta/countK*summ2[a];
                        }
                    }
                }//for(int i=0; i<numOfLis; i++)
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    //v[j][a]=v[j][a]-leaRat*summ[a]*v[j][a];
                    v[j][a]=v[j][a]-leaRat*summ[a];
                }
            } //for(int j=0; j<numOfDoc; j++)
        }//for(int ite=0; ite<maxIteTim_; ite++)
        /////////////////////////
        /*
         for(int j=0; j<numOfDoc; j++)
         {
         svLis=0.0;
         for(int i=0; i<numOfLis; i++)
         {
         if(Iij[i][j]>0)
         {
         for(int a=0; a<numOfAsp; a++)
         {
         svLis=svLis+s[a][i]*v[j][a];
         }
         }
         }//for(int i=0; i<numOfLis; i++)
         e[j]=svLis;
         sigma[j]=0.0; //initialize sigma
         for(int i=0; i<numOfLis; i++)
         {
         if(Iij[i][j]<=0)
         {
         sigma[j]=sigma[j]+1.0/numOfLis*pow(0.0-e[j]/numOfLis, 2.0);
         }
         else
         {
         sv=0.0;
         for(int a=0; a<numOfAsp; a++)
         {
         sv=sv+s[a][i]*v[j][a];
         }
         sigma[j]=sigma[j]+1.0/numOfLis*pow(sv-e[j]/numOfLis, 2.0);
         }
         }
         //std::cout<<"expactation="<<e[j]<<"  sigma="<<sigma[j]<<std::endl;
         }//for(int j=0; j<numOfDoc; j++)
         for(int j=0; j<numOfDoc; j++)
         {
         for(int j2=0; j2<numOfDoc; j2++)
         {
         rho[j][j2]=0.0; //initialize rho
         for(int i=0; i<numOfLis; i++)
         {
         rho_sv=0.0;
         rho_sv2=0.0;
         if(Iij[i][j]<=0.0)
         {
         rho_sv=0.0;
         }
         else
         {
         for(int a=0; a<numOfAsp; a++)
         {
         rho_sv=rho_sv+s[a][i]*v[j][a];
         }
         }
         if(Iij[i][j2]<=0.0)
         {
         rho_sv2=0.0;
         }
         else
         {
         for(int a=0; a<numOfAsp; a++)
         {
         rho_sv2=rho_sv2+s[a][i]*v[j2][a];
         }
         }
         rho[j][j2]=rho[j][j2]+1.0/numOfLis*(rho_sv-1.0/numOfLis*e[j])*(rho_sv2-1.0/numOfLis*e[j2]);
         }
         //std::cout<<"expactation="<<e[j]<<"  coviariance="<<rho[j][j2]<<std::endl;
         }//for(int j2=0; j2<numOfDoc; j2++)
         }//for(int j=0; j<numOfDoc; j++)
         for(int j=0; j<numOfDoc; j++)
         {
         indCovTemp.ind=j;
         indCovTemp.cov=0.0; //sum_{i=1}^{k-1}
         indCovVec.push_back(indCovTemp);
         }
         for(int j=0; j<numOfDoc; j++)
         {
         max=0.0;
         for(itIndCovVec=indCovVec.begin(); itIndCovVec!=indCovVec.end(); itIndCovVec++)
         {
         maxTemp=e[itIndCovVec->ind]-bWeight*1.0/log2(j+1.0+1.0)*sigma[itIndCovVec->ind]-2.0*bWeight*1.0/log2(j+1.0+1.0)*itIndCovVec->cov;
         if(maxTemp>max)
         {
         itIndCovVec2=itIndCovVec;
         max=maxTemp;
         }
         }
         indVec.push_back(itIndCovVec2->ind);
         for(int j2=0; j2<indCovVec.size(); j2++)
         {
         indCovVec[j2].cov=indCovVec[j2].cov+rho[itIndCovVec2->ind][j2];
         }
         indCovVec.erase(itIndCovVec2);
         }//for(int j=0; j<numOfDoc; j++)
         
         for(int j=0; j<numOfDoc; j++)
         {
         index=indVec[j];
         itDocIndDoc=docIndDoc.find(index);
         //docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, numOfDoc-j));
         }
         */
        /////////////////
        
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    if(Iij[i][j]>0)
                    {
                        svLis=svLis+s[a][i]*v[j][a];
                    }
                }
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLis));
        }
        
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
}

void
lss::ModelBurst::computQueDocScoBurst5(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::DOCSCO_T::iterator itDSj; //itDS for j-th doc
    lss::DOCSCO_T::iterator itDSk; //itDS for k-th doc
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::DOCDOCIND_T::iterator itDocDocInd;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco; //query-doc-doc-dist
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    lss::DOCSCO_T::iterator itDocSco;
    lss::QUEDOCSCO_T::iterator itQTDSSUM;
    lss::DOCSCO_T::iterator itTDSSUM;
    lss::INDCOV_T indCovTemp;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    const double beta=beta_;
    const double bWeight=bWeight_;
    double cost=0.0;
    double cost2=0.0;
    double sv=0.0;
    double isv=0.0; //sum I S V
    double ir=0.0; // sum I R
    //double Iij=0.0;
    double sumn[numOfAsp];
    double sumn2[numOfAsp];
    double summ[numOfAsp];
    double summ2[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    int k_ind=0;
    int k=0;
    double countK=0.0;
    bool hasRead=false;
    double max=0.0;
    double maxTemp=0.0;
    int maxIndTemp=0;
    double svLisSum=0.0;
    
    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCDOCIND_T docDocInd;
        lss::DOCSCO_T docSco;
        std::vector<lss::INDCOV_T> indCovVec; //index-covirance vector
        std::vector<lss::INDCOV_T>::iterator itIndCovVec;
        std::vector<lss::INDCOV_T>::iterator itIndCovVec2;
        std::vector<int> indVec; //final ranking document index vector
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        double e[numOfDoc]; //expectation
        double sigma[numOfDoc]; //variance
        double rho[numOfDoc][numOfDoc]; //covariance
        double rho_sv;
        double rho_sv2;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        itQDocDocSco=bur.qDocDocSco.find(itQAllDocInd->first);
        if(itQDocDocSco==bur.qDocDocSco.end())
        {
            std::cout<<"There is an erro in qDocDocSco in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        itQTDSSUM=qTDSSUM_.find(itQAllDocInd->first);
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            docDocInd.insert(lss::DOCDOCIND_PAIR_T(itAllDocInd->first, index));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.05;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.05;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-gF(sv), 2.0);
                }
            }
            
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn[a]=sumn[a]+Iij[i][j]*(gF(sv)-r[i][j])*gDerF(sv)*v[j][a];
                    }
                }
                
                for(int j=0; j<numOfDoc; j++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn2[a]=0.0;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    itQDS=runs.qDS[i].find(itQAllDocInd->first);
                    itDSj=itQDS->second.find(itDocIndDoc->second);
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDSj->first<<" "<<itDSj->second<<" "<<itDSk->first<<" "<<itDSk->second<<std::endl;
                            continue;
                        }
                        countK=countK+1.0;
                        cost=cost+itDocSco->second*Iij[i][k]*pow((r[i][k]-gF(sv)),2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn2[a]=sumn2[a]+itDocSco->second*Iij[i][k]*(gF(sv)-r[i][k])*gDerF(sv)*v[j][a];
                            //std::cout<<"r="<<r[i][k]<<" gf="<<gF(sv)<<std::endl;
                            //std::cout<<sv-r[i][k]<<" sv="<<sv<<" r="<<r[i][k]<<std::endl;
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        /*for(int a=0; a<numOfAsp; a++)
                        {
                            sumn[a]=sumn[a]+beta*Iij[i][j]*(sv-r[i][j])*v[j][a];
                        }*/
                        continue;
                    }
                    else
                    {
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        cost=cost+Iij[i][j]*pow((r[i][j]-gF(sv)), 2.0);
                        for(int a=0; a<numOfAsp; a++)
                        {
                            sumn[a]=sumn[a]+beta/countK*sumn2[a];
                            //std::cout<<sumn[a]<<"  "<<beta/countK*sumn2[a]<<std::endl;
                        }
                    }
                }//for(int j=0; j<numOfDoc; j++)
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    //s[a][i]=s[a][i]-leaRat*sumn[a]*s[a][i];
                    s[a][i]=s[a][i]-leaRat*sumn[a];
                    //std::cout<<summ[a]<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            if(cost<cost2)
            {
                //std::cout<<"cost="<<cost<<std::endl;
            }
            cost2=cost;
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+Iij[i][j]*(gF(sv)-r[i][j])*gDerF(sv)*s[a][i];
                        //std::cout<<"sv="<<sv<<" gf="<<gF(sv)<<" gDerF="<<gDerF(sv)<<std::endl;
                    }
                }
                
                for(int i=0; i<numOfLis; i++)
                {
                    if(Iij[i][j]<=0.0)
                    {
                        continue;
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ2[a]=0.0;
                    }
                    itDocIndDoc=docIndDoc.find(j);
                    itDocDocSco=itQDocDocSco->second.find(itDocIndDoc->second);
                    countK=0.0;
                    for(itDocSco=itDocDocSco->second.begin(); itDocSco!=itDocDocSco->second.end(); itDocSco++)
                    {
                        itDocDocInd=docDocInd.find(itDocSco->first);
                        k=itDocDocInd->second;
                        if(Iij[i][k]<=0.0)
                        {
                            continue;
                        }
                        itQDS=runs.qDS[i].find(itQAllDocInd->first);
                        itDSj=itQDS->second.find(itDocIndDoc->second);
                        itDSk=itQDS->second.find(itDocSco->first);
                        if(itDSj==itQDS->second.end() || itDSk==itQDS->second.end())
                        {
                            std::cout<<"There is an error in runs.qDS."<<std::endl;
                            return;
                        }
                        if(itDSj->second <= itDSk->second) //j-th doc ranks high than k-th doc, or j=k
                        {
                            continue;
                        }
                        countK=countK+1.0;
                        sv=0.0;
                        for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                        {
                            sv=sv+s[a][i]*v[j][a];
                        }
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ2[a]=summ2[a]+itDocSco->second*Iij[i][k]*(gF(sv)-r[i][k])*gDerF(sv)*s[a][i];
                        }
                    }//for(itDocSco=itDocDocSco->second.begin();
                    if(countK<=0)
                    {
                        /*for(int a=0; a<numOfAsp; a++)
                        {
                            summ[a]=summ[a]+beta*Iij[i][j]*(sv-r[i][j])*s[a][i];
                        }*/
                        continue;
                    }
                    else
                    {
                        for(int a=0; a<numOfAsp; a++)
                        {
                            summ[a]=summ[a]+beta/countK*summ2[a];
                        }
                    }
                }//for(int i=0; i<numOfLis; i++)
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    //v[j][a]=v[j][a]-leaRat*summ[a]*v[j][a];
                    v[j][a]=v[j][a]-leaRat*summ[a];
                }
            } //for(int j=0; j<numOfDoc; j++)
            //std::cout<<ite<<" "<<cost<<std::endl;
        }//for(int ite=0; ite<maxIteTim_; ite++)
        /////////////////////////
        /*
         for(int j=0; j<numOfDoc; j++)
         {
         svLis=0.0;
         for(int i=0; i<numOfLis; i++)
         {
         if(Iij[i][j]>0)
         {
         for(int a=0; a<numOfAsp; a++)
         {
         svLis=svLis+s[a][i]*v[j][a];
         }
         }
         }//for(int i=0; i<numOfLis; i++)
         e[j]=svLis;
         sigma[j]=0.0; //initialize sigma
         for(int i=0; i<numOfLis; i++)
         {
         if(Iij[i][j]<=0)
         {
         sigma[j]=sigma[j]+1.0/numOfLis*pow(0.0-e[j]/numOfLis, 2.0);
         }
         else
         {
         sv=0.0;
         for(int a=0; a<numOfAsp; a++)
         {
         sv=sv+s[a][i]*v[j][a];
         }
         sigma[j]=sigma[j]+1.0/numOfLis*pow(sv-e[j]/numOfLis, 2.0);
         }
         }
         //std::cout<<"expactation="<<e[j]<<"  sigma="<<sigma[j]<<std::endl;
         }//for(int j=0; j<numOfDoc; j++)
         for(int j=0; j<numOfDoc; j++)
         {
         for(int j2=0; j2<numOfDoc; j2++)
         {
         rho[j][j2]=0.0; //initialize rho
         for(int i=0; i<numOfLis; i++)
         {
         rho_sv=0.0;
         rho_sv2=0.0;
         if(Iij[i][j]<=0.0)
         {
         rho_sv=0.0;
         }
         else
         {
         for(int a=0; a<numOfAsp; a++)
         {
         rho_sv=rho_sv+s[a][i]*v[j][a];
         }
         }
         if(Iij[i][j2]<=0.0)
         {
         rho_sv2=0.0;
         }
         else
         {
         for(int a=0; a<numOfAsp; a++)
         {
         rho_sv2=rho_sv2+s[a][i]*v[j2][a];
         }
         }
         rho[j][j2]=rho[j][j2]+1.0/numOfLis*(rho_sv-1.0/numOfLis*e[j])*(rho_sv2-1.0/numOfLis*e[j2]);
         }
         //std::cout<<"expactation="<<e[j]<<"  coviariance="<<rho[j][j2]<<std::endl;
         }//for(int j2=0; j2<numOfDoc; j2++)
         }//for(int j=0; j<numOfDoc; j++)
         for(int j=0; j<numOfDoc; j++)
         {
         indCovTemp.ind=j;
         indCovTemp.cov=0.0; //sum_{i=1}^{k-1}
         indCovVec.push_back(indCovTemp);
         }
         for(int j=0; j<numOfDoc; j++)
         {
         max=0.0;
         for(itIndCovVec=indCovVec.begin(); itIndCovVec!=indCovVec.end(); itIndCovVec++)
         {
         maxTemp=e[itIndCovVec->ind]-bWeight*1.0/log2(j+1.0+1.0)*sigma[itIndCovVec->ind]-2.0*bWeight*1.0/log2(j+1.0+1.0)*itIndCovVec->cov;
         if(maxTemp>max)
         {
         itIndCovVec2=itIndCovVec;
         max=maxTemp;
         }
         }
         indVec.push_back(itIndCovVec2->ind);
         for(int j2=0; j2<indCovVec.size(); j2++)
         {
         indCovVec[j2].cov=indCovVec[j2].cov+rho[itIndCovVec2->ind][j2];
         }
         indCovVec.erase(itIndCovVec2);
         }//for(int j=0; j<numOfDoc; j++)
         
         for(int j=0; j<numOfDoc; j++)
         {
         index=indVec[j];
         itDocIndDoc=docIndDoc.find(index);
         //docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, numOfDoc-j));
         }
         */
        /////////////////
        
        for(int j=0; j<numOfDoc; j++)
        {
            svLisSum=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                svLis=0.0;
                for(int a=0; a<numOfAsp; a++)
                {
                    if(Iij[i][j]>0)
                    {
                        svLis=svLis+s[a][i]*v[j][a];
                    }
                }
                svLisSum=svLisSum+gF(svLis);
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLisSum));
        }
        
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
}

void
lss::ModelBurst::wriEvaResult()
{
    lss::ResultWriter resWri;
    std::string savFil=lemur::api::ParamGetString("burstComb", "burst");
    resWri.writeResult(qDSco_, savFil);
}