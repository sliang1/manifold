//
//  Manifold.cpp
//  Fusion
//
//  Created by Shangsong Liang on 9/18/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "ManifoldCluster.h"
#include "ResultWriter.h"
// Lemur
#include "Param.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <omp.h>

lss::ManifoldCluster::ManifoldCluster(lss::CombSUMMNZ combSUMMNZ)
{
    this->initSmoMetPar();
    this->qDS_sum_=combSUMMNZ.getQDSSUM();
    this->qDS_mnz_=combSUMMNZ.getQDSMNZ();
    this->openIndex();
    this->wriQDSManClu();
    this->closeIndex();
    this->wriEvaResult();
}

lss::ManifoldCluster::~ManifoldCluster()
{
    
}

void
lss::ManifoldCluster::initSmoMetPar()
{
    this->smoMet_=lemur::api::ParamGetString("smoothing");
    double smoPar=0.0;
    int intTemp=lemur::api::ParamGet("smoothingPar", smoPar);
    this->smoPar_=smoPar;
    double paramScor=0.0;
    int tSSDN=lemur::api::ParamGet("topStanScoreDocNum", paramScor, -1);
    this->tSSDN_=paramScor;
    double alpha=lemur::api::ParamGet("alpha", paramScor, 0.99);
    this->alpha_=paramScor;
    int maxInt=lemur::api::ParamGet("maxInteration", paramScor, 30);
    this->maxInt_=paramScor;
    intTemp=lemur::api::ParamGet("aveSquErr", paramScor, 0.00000001);
    this->aveSquErr_=paramScor;
    intTemp=lemur::api::ParamGet("delta", paramScor, 10);
    this->delta_=paramScor;
    intTemp=lemur::api::ParamGet("lambda", paramScor, 0.5);
    this->lambda_=paramScor;
}
void
lss::ManifoldCluster::openIndex()
{
    std::string indFil=lemur::api::ParamGetString("index");
    if(indFil.length()<=0)
    {
        std::cout<<"Parameter 'index' is not set."<<std::endl;
        return;
    }
    try 
    {
        index_=lemur::api::IndexManager::openIndex(indFil);
    } 
    catch (const lemur::api::Exception& e)
    {
        std::cout<<"Can't open index, check parameter 'index'. "<<std::endl;
    }
}

void
lss::ManifoldCluster::closeIndex()
{
    if(index_!=NULL)
    {
        delete index_;
        index_=0;
    }
}

double
lss::ManifoldCluster::computeKLScor(std::string doc1, std::string doc2)
{
    int docID1=index_->document(doc1);
    int docID2=index_->document(doc2);
    double docLen1=index_->docLength(docID1);
    double docLen2=index_->docLength(docID2);
    double skLScor=0.0;//symmetric K-L divergence score
    
    lemur::langmod::DocUnigramCounter* collCounter1 
    = new lemur::langmod::DocUnigramCounter(docID1,*index_ );
    lemur::langmod::DocUnigramCounter* collCounter2
    = new lemur::langmod::DocUnigramCounter(docID2, *index_);
    lemur::langmod::DocUnigramCounter* collCounter
    =new lemur::langmod::DocUnigramCounter(*index_);
    
    lemur::langmod::UnigramLM* collModel1 
    = new lemur::langmod::MLUnigramLM( *collCounter1, index_->termLexiconID() );
    lemur::langmod::UnigramLM *collModel2
    = new lemur::langmod::MLUnigramLM( *collCounter2, index_->termLexiconID() );
    lemur::langmod::UnigramLM *collModel
    = new lemur::langmod::MLUnigramLM( *collCounter, index_->termLexiconID() );
    
    int termID=0;
    double prob=0.0;
    double probSum=0.0;
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double muPwC=0.0;//mu*p(w|c)
    double sumWInQ=0.0;//term in Q
    double sumWInDNotInQ=0.0;//term in D but not in Q
    double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
    std::string termStr;
    collModel1->startIteration();
    for(;collModel1->hasMore();)
    {
        collModel1->nextWordProb(termID, prob);
        muPwC=this->smoPar_*collModel->prob(termID);
        if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
        {
            smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
            smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
            sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
        }
    }
    skLScor=sumWInQ;
    collModel2->startIteration();
    for(;collModel2->hasMore();)
    {
        collModel2->nextWordProb(termID, prob);
        muPwC=this->smoPar_*collModel->prob(termID);
        if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
        {
            smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
            smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
            sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
        }
    }
    skLScor=skLScor+sumWInDNotInQ;
    delete collCounter1;
    collCounter1=0;
    delete collCounter2;
    collCounter2=0;
    delete collModel1;
    collModel1=0;
    delete collModel2;
    collModel2=0;
    return 0.5*skLScor;
}

double
lss::ManifoldCluster::computeKLScor2(std::string doc1, std::string doc2)
{
    int docID1=index_->document(doc1);
    int docID2=index_->document(doc2);
    double docLen1=index_->docLength(docID1);
    double docLen2=index_->docLength(docID2);
    double skLScor=0.0;//symmetric K-L divergence score
    
    ////////////
    lemur::langmod::DocUnigramCounter collCounter1(docID1,*index_ );
    lemur::langmod::DocUnigramCounter collCounter2(docID2, *index_);
    lemur::langmod::DocUnigramCounter collCounter(*index_);
    lemur::langmod::MLUnigramLM mLULM1(collCounter1, index_->termLexiconID() );
    lemur::langmod::MLUnigramLM mLULM2(collCounter2, index_->termLexiconID() );
    lemur::langmod::MLUnigramLM mLULM(collCounter, index_->termLexiconID() );
    lemur::langmod::UnigramLM* collModel1=&mLULM1;
    lemur::langmod::UnigramLM* collModel2=&mLULM2;
    lemur::langmod::UnigramLM* collModel=&mLULM;
    
    ///////////
    
    
    int termID=0;
    double prob=0.0;
    double probSum=0.0;
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double muPwC=0.0;//mu*p(w|c)
    double sumWInQ=0.0;//term in Q
    double sumWInDNotInQ=0.0;//term in D but not in Q
    double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
    std::string termStr;
    collModel1->startIteration();
    for(;collModel1->hasMore();)
    {
        collModel1->nextWordProb(termID, prob);
        muPwC=this->smoPar_*collModel->prob(termID);
        if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
        {
            smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
            smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
            sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
        }
    }
    skLScor=sumWInQ;
    collModel2->startIteration();
    for(;collModel2->hasMore();)
    {
        collModel2->nextWordProb(termID, prob);
        muPwC=this->smoPar_*collModel->prob(termID);
        if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
        {
            smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
            smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
            sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
        }
    }
    skLScor=skLScor+sumWInDNotInQ;
    //delete collCounter1;
    //collCounter1=0;
    //delete collCounter2;
    //collCounter2=0;
    //std::cout<<"^^^^^^^^^"<<std::endl;
    //delete collModel1;
    //std::cout<<"^^^^^^^^^!!!!"<<std::endl;
    collModel1=0;
    //delete collModel2;
    collModel2=0;
    //delete collModel;
    collModel=0;
    return 0.5*skLScor;
}

void
lss::ManifoldCluster::wriQDSManClu()
{
    lss::QUEDOCSCO_T::iterator it;
    lss::QUEDOCSCO_T::iterator it2;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSCO_T::iterator it_dS2;
    lss::DOCCHA_T::iterator it_dC;
    lss::DOCCHA_T::iterator it_dC2;
    lss::DOCDOCSCO_T::iterator it_dDS;
    double skLScor=0.0;
    std::cout<<"Total number of query is: "<<this->qDS_sum_.size()<<std::endl;
    for(it=this->qDS_sum_.begin(); it!=this->qDS_sum_.end(); it++)
    {
        
        /*if(it->first=="151")
        {
            std::cout<<"Start fusing the lists for the query: "<<it->first<<"... ..."<<std::endl;
            std::cout<<"Computing document-document symmetric K-L scores... ..."<<std::endl;
            this->wriWQDSManForSpeQue(it->first);
            this->wriQDSCluForSpeQue(it->first);
            std::cout<<"Finish fusing the lists for the query: "<<it->first<<"."<<std::endl;
            std::cout<<std::endl;
            this->deleteW_();
        }*/
         
        std::cout<<"Start fusing the lists for the query: "<<it->first<<"... ..."<<std::endl;
        std::cout<<"Computing document-document symmetric K-L scores... ..."<<std::endl;
        this->wriWQDSManForSpeQue(it->first);
        this->wriQDSCluForSpeQue(it->first);
        std::cout<<"Finish fusing the lists for the query: "<<it->first<<"."<<std::endl;
        std::cout<<std::endl;
        this->deleteW_();
    }
}

void
lss::ManifoldCluster::wriWQDSManForSpeQue(std::string queStr)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::QUEDOCSCO_T::iterator it2;
    lss::QUEDOCSCO_T::iterator it_man_sum;
    lss::QUEDOCSCO_T::iterator it_man_mnz;
    lss::QUEDOCSCO_T::iterator it_clu_sum;
    lss::QUEDOCSCO_T::iterator it_clu_mnz;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSCO_T::iterator it_dS2;
    lss::DOCSCO_T::iterator it_dDS_dS;
    lss::DOCDOCSCO_T dDS;//document-document-kLScore
    lss::DOCDOCSCO_T::iterator it_dDS;
    lss::DOCSCO_T dS_man_sum;//document-manifold based ranking score using combSUM
    lss::DOCSCO_T dS_man_mnz;//document-manifold based ranking score using combMNZ
    lss::DOCSCO_T dS_clu_sum;//document-cluster based ranking score using combSUM
    lss::DOCSCO_T dS_clu_mnz;//document-cluster based ranking score using combMNZ
    
    it=this->qDS_sum_.find(queStr);
    double skLScor=0.0;
    //double** W;
    const int sizMat=it->second.size();
    double D[sizMat][sizMat];
    double S[sizMat][sizMat];
    double MatTem[sizMat][sizMat];//a temporal metrix
    double Y_sum[sizMat];
    double Y_mnz[sizMat];
    double Ft_sum[sizMat];//F(t) based on CombSUM
    double Ft2_sum[sizMat];//F(t+1) based on CombSUM
    double Ft_mnz[sizMat];//F(t) based on CombMNZ
    double Ft2_mnz[sizMat];//F(t+1) based on CombMNZ
    double thr_sum=0.0;//threshold ranking score for CombSUM
    double thr_mnz=0.0;//threshold ranking score for CombMNZ
    double thr_err=0.0;
    double sum_err=0.0;
    //int sizMat=0;//size of the matrix for W, D and S
    int row=0;//the row-th row in a matrix
    int col=0;//the col-th column in a matrix
    int ind=0;
    double sKLScorMat=0.0;//symmetric K-L score in a matrix
    double sumRowExpKLScor=0.0;
    double ent=0.0;//entity score in a Matrix
    double ent2=0.0;//entity score in a Matrix
    std::vector<double> scoVec_sum;//put CombSUM scores inot a vector
    std::vector<double> scoVec_mnz;//put CombMNZ scores into a vector
    
    int termID=0;
    double prob=0.0;
    double probSum=0.0;
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double muPwC=0.0;//mu*p(w|c)
    double sumWInQ=0.0;//term in Q
    double sumWInDNotInQ=0.0;//term in D but not in Q
    double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
    std::string termStr;
    lemur::langmod::DocUnigramCounter collCounter(*index_);
    lemur::langmod::MLUnigramLM mLULM(collCounter, index_->termLexiconID() );
    lemur::langmod::UnigramLM* collModel=&mLULM;
    
    for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    {
        it_dS2=it_dS;
        it_dS2++;
        int docID1=index_->document(it_dS->first);
        double docLen1=index_->docLength(docID1);
        lemur::langmod::DocUnigramCounter collCounter1(docID1, *index_);
        lemur::langmod::MLUnigramLM mLULM1(collCounter1, index_->termLexiconID() );
        lemur::langmod::UnigramLM* collModel1=&mLULM1;
        for(; it_dS2!=it->second.end(); it_dS2++)
        {
            int docID2=index_->document(it_dS2->first);
            double docLen2=index_->docLength(docID2);
            lemur::langmod::DocUnigramCounter collCounter2(docID2, *index_);
            lemur::langmod::MLUnigramLM mLULM2(collCounter2, index_->termLexiconID() );
            lemur::langmod::UnigramLM* collModel2=&mLULM2;
            skLScor=0.0;
            smoPro1=0.0;
            smoPro2=0.0;
            muPwC=0.0;
            sumWInQ=0.0;
            sumWInDNotInQ=0.0;
            sumWOutQAndD=0.0;
            collModel1->startIteration();
            for(;collModel1->hasMore();)
            {
                collModel1->nextWordProb(termID, prob);
                muPwC=this->smoPar_*collModel->prob(termID);
                if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
                {
                    smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
                    smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
                    sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
                }
            }
            skLScor=sumWInQ;
            collModel2->startIteration();
            for(;collModel2->hasMore();)
            {
                collModel2->nextWordProb(termID, prob);
                muPwC=this->smoPar_*collModel->prob(termID);
                if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
                {
                    smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
                    smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
                    sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
                }
            }
            skLScor=0.5*(skLScor+sumWInDNotInQ);
            ////
            //skLScor=this->computeKLScor2(it_dS->first, it_dS2->first);
            it_dDS=dDS.find(it_dS->first);
            if(it_dDS==dDS.end())
            {
                lss::DOCSCO_T dS;
                dS.insert(lss::DOCSCO_PAIR_T(it_dS2->first, skLScor));
                dDS.insert(lss::DOCDOCSCO_PAIR_T(it_dS->first, dS));
                std::cout<<"query="<<queStr<<" doc="<<it_dS->first<<" doc="<<it_dS2->first<<" sKL="<<skLScor<<std::endl;
            }
            else
            {
                it_dDS->second.insert(lss::DOCSCO_PAIR_T(it_dS2->first, skLScor));
                std::cout<<"query="<<queStr<<" doc="<<it_dS->first<<" doc="<<it_dS2->first<<" sKL="<<skLScor<<std::endl;
            }
            collModel2=0;
        }//for(; it_dS2!=it->second.end(); it_dS2++)
        collModel1=0;
    }//for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    this->sizMat_=sizMat;
    W_=new double*[sizMat];
    //D=new double*[sizMat];
    //S=new double*[sizMat];
    //MatTem=new double*[sizMat];
    //Y_sum=new double[sizMat];
    //Y_mnz=new double[sizMat];
    //Ft_sum=new double[sizMat];
    //Ft2_sum=new double[sizMat];
    //Ft_mnz=new double[sizMat];
    //Ft2_mnz=new double[sizMat];
    for(int i=0; i<sizMat; i++)
    {
        W_[i]=new double[sizMat];
        //D[i]=new double[sizMat];
        //S[i]=new double[sizMat];
        //MatTem[i]=new double[sizMat];
    }
    row=0;
    for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    {
        it_dS2=it_dS;
        it_dS2++;
        W_[row][row]=0.0;//W[row][row]=0.0
        col=row+1;
        it_dDS=dDS.find(it_dS->first);
        if(it_dDS==dDS.end()&& row<sizMat-1)
        {
            std::cout<<"There is an error in dDS."<<std::endl;
            continue;
        }
        for(; it_dS2!=it->second.end(); it_dS2++)
        {
            it_dDS_dS=it_dDS->second.find(it_dS2->first);
            if(it_dDS_dS==it_dDS->second.end())
            {
                continue;
            }
            skLScor=it_dDS_dS->second;
            sKLScorMat=exp(-skLScor);
            W_[row][col]=sKLScorMat;
            W_[col][row]=sKLScorMat;
            col=col+1;
            //std::cout<<"doc*="<<it_dS->first<<" doc*="<<it_dS2->first<<" sKL="<<skLScor<<" sco="<<exp(-skLScor)<<std::endl;
        }
        row=row+1;
    }//for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    for(int i=0; i<sizMat; i++)
    {
        sumRowExpKLScor=0.0;
        for(int j=0; j<sizMat; j++)
        {
            sumRowExpKLScor=sumRowExpKLScor+W_[i][j];
            if(i!=j)
            {
                D[i][j]=0.0;
            }
        }
        D[i][i]=sqrt(1.0/sumRowExpKLScor);//D^{-0.5}
    }
    
    for(int i=0; i<sizMat; i++)//MatTem=D^{-0.5}*W
    {
        for(int j=0; j<sizMat; j++)
        {
            ent=0.0;
            for(int k=0; k<sizMat; k++)
            {
                ent=ent+D[i][k]*W_[k][j];
            }
            MatTem[i][j]=ent;
        }//for(int j=0; j<sizMat; j++)
    }//for(int i=0; i<sizMat; i++)
    for(int i=0; i<sizMat; i++)//S=MatTem*D^{-0.5}=D^{-0.5}*W*D^{-0.5}
    {
        for(int j=0; j<sizMat; j++)
        {
            ent=0.0;
            for(int k=0; k<sizMat; k++)
            {
                ent=ent+MatTem[i][k]*D[k][j];
            }
            S[i][j]=ent;
        }
    }
    for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    {
        scoVec_sum.push_back(it_dS->second);
    }
    std::sort(scoVec_sum.rbegin(), scoVec_sum.rend());
    it2=this->qDS_mnz_.find(queStr);
    for(it_dS2=it2->second.begin(); it_dS2!=it2->second.end(); it_dS2++)
    {
        scoVec_mnz.push_back(it_dS2->second);
    }
    std::sort(scoVec_mnz.rbegin(), scoVec_mnz.rend());
    if(this->tSSDN_>0 && this->tSSDN_<sizMat)
    {
        thr_sum=scoVec_sum[this->tSSDN_-1];
        thr_mnz=scoVec_mnz[this->tSSDN_-1];
    }
    else
    {
        thr_sum=-1.0;
        thr_mnz=-1.0;
    }
    ind=0;
    for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    {
        if(it_dS->second>=thr_sum)
        {
            Y_sum[ind]=it_dS->second/scoVec_sum[0];
            //Y_sum[ind]=it_dS->second;
            //Y_sum[ind]=1.0;
        }
        else
        {
            Y_sum[ind]=0.0;
        }
        ind=ind+1;
    }
    ind=0;
    for(it_dS2=it2->second.begin(); it_dS2!=it2->second.end(); it_dS2++)
    {
        if(it_dS2->second>=thr_mnz)
        {
            Y_mnz[ind]=it_dS2->second/scoVec_mnz[0];
            //Y_mnz[ind]=it_dS2->second;
            //Y_mnz[ind]=1.0;
        }
        else
        {
            Y_mnz[ind]=0.0;
        }
        ind=ind+1;
    }
    for(int i=0; i<sizMat; i++)
    {
        Ft_sum[i]=Y_sum[i];
        Ft_mnz[i]=Y_mnz[i];
    }
    thr_err=this->aveSquErr_ * sizMat;
    for(int i=0; i<this->maxInt_; i++)
    {
        for(int j=0; j<sizMat; j++)
        {
            ent=0.0;
            for(int k=0; k<sizMat; k++)
            {
                ent=ent+this->alpha_*S[j][k]*Ft_sum[k];
            }//for(int k=0; k<sizMat; k++)
            ent=ent+(1.0-this->alpha_)*Y_sum[j];
            Ft2_sum[j]=ent;
        }//for(int j=0; j<sizMat; j++)
        sum_err=0.0;
        for(int m=0; m<sizMat; m++)
        {
            sum_err=sum_err+pow((Ft2_sum[m]-Ft_sum[m]), 2.0);
            Ft_sum[m]=Ft2_sum[m];
        }
        if(sum_err<=thr_err)
        {
            std::cout<<"sum_err="<<sum_err<<std::endl;
            std::cout<<"Iteration times is: "<<i<<", square error="<<sum_err<<std::endl;
            break;
        }
        //std::cout<<"sum_err="<<sum_err<<std::endl;
    }//for(int i=0; i<this->maxInt_; i++)
    for(int i=0; i<this->maxInt_; i++)
    {
        for(int j=0; j<sizMat; j++)
        {
            ent2=0.0;
            for(int k=0; k<sizMat; k++)
            {
                ent2=ent2+this->alpha_*S[j][k]*Ft_mnz[k];
            }//for(int k=0; k<sizMat; k++)
            ent2=ent2+(1.0-this->alpha_)*Y_mnz[j];
            Ft2_mnz[j]=ent2;
        }//for(int j=0; j<sizMat; j++)
        sum_err=0.0;
        for(int m=0; m<sizMat; m++)
        {
            sum_err=sum_err+pow((Ft2_mnz[m]-Ft_mnz[m]), 2.0);
            Ft_mnz[m]=Ft2_mnz[m];
        }
        if(sum_err<=thr_err)
        {
            std::cout<<"sum_err="<<sum_err<<std::endl;
            std::cout<<"Iteration times is: "<<i<<", square error="<<sum_err<<std::endl;
            break;
        }
        //std::cout<<"sum_err="<<sum_err<<std::endl;
    }//for(int i=0; i<this->maxInt_; i++)
    ind=0;
    it_man_sum=this->qDS_man_sum_.find(queStr);
    if(it_man_sum==this->qDS_man_sum_.end())
    {
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            dS_man_sum.insert(lss::DOCSCO_PAIR_T(it_dS->first, Ft2_sum[ind]));
            ind=ind+1;
        }
        this->qDS_man_sum_.insert(lss::QUEDOCSCO_PAIR_T(queStr, dS_man_sum));
    }
    ind=0;
    it_man_mnz=this->qDS_man_mnz_.find(queStr);
    if(it_man_mnz==this->qDS_man_mnz_.end())
    {
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            dS_man_mnz.insert(lss::DOCSCO_PAIR_T(it_dS->first, Ft2_mnz[ind]));
            ind=ind+1;
        }
        this->qDS_man_mnz_.insert(lss::QUEDOCSCO_PAIR_T(queStr, dS_man_mnz));
    }
    //for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
    //{
        //std::cout<<"doc="<<it_dS->first<<" CombSUM="<<it_dS->second<<std::endl;
        //std::cout<<"doc="<<it_dS->first<<" Ft2="<<Ft2_sum[ind]<<std::endl;
        //ind=ind+1;
    //}
    //for(int i=0; i<sizMat; i++)
    //{
        //std::cout<<Ft2_sum[i]<<std::endl;
    //}
    
    //for(int i=0; i<sizMat; i++)
    //{
        //if(W[i]!=NULL)
        //{
            //delete[] W[i];
        //}
        //if(D[i]!=NULL)
        //{
            //delete[] D[i];
            //D[i]=NULL;
        //}
        //if(S[i]!=NULL)
        //{
            //delete[] S[i];
            //S[i]=NULL;
        //}
        //if(MatTem[i]!=NULL)
        //{
            //delete[] MatTem[i];
            //MatTem[i]=NULL;
        //}
    //}//for(int i=0; i<sizMat; i++)
    //delete[] W;
    //delete[] D;
    //D=NULL;
    //delete[] S;
    //S=NULL;
    //delete[] MatTem;
    //MatTem=NULL;
    /*if(Y_sum!=NULL)
    {
        delete[] Y_sum;
        Y_sum=NULL;
    }
    if(Y_mnz!=NULL)
    {
        delete[] Y_mnz;
        Y_mnz=NULL;
    }
    if(Ft_sum!=NULL)
    {
        delete[] Ft_sum;
        Ft_sum=NULL;
    }
    if(Ft2_sum!=NULL)
    {
        delete[] Ft2_sum;
        Ft2_sum=NULL;
    }
    if(Ft_mnz!=NULL)
    {
        delete[] Ft_mnz;
        Ft_mnz=NULL;
    }
    if(Ft2_mnz!=NULL)
    {
        delete[] Ft2_mnz;
        Ft2_mnz=NULL;
    }*/
    collModel=0;
}

void
lss::ManifoldCluster::wriQDSCluForSpeQue(std::string queStr)
{
    lss::DOCSCO_T dS_sum;//p(d|q) based on CombSUM
    lss::DOCSCO_T dS_mnz;//p(d|q) based on CombMNZ
    lss::QUEDOCSCO_T::iterator it_sum;
    lss::QUEDOCSCO_T::iterator it_mnz;
    lss::QUEDOCSCO_T::iterator it_clu_sum;
    lss::QUEDOCSCO_T::iterator it_clu_mnz;
    lss::DOCSCO_T::iterator it_dS_sum;
    lss::DOCSCO_T::iterator it_dS_mnz;
    double W[sizMat_][sizMat_];
    double cInf[sizMat_][sizMat_];//cluster information matrix
    double pdq_sum[sizMat_];
    double pdq_mnz[sizMat_];
    double pcq_sum[sizMat_];
    double pcq_mnz[sizMat_];
    double thr[sizMat_];
    double Fdq_sum[sizMat_];
    double Fdq_mnz[sizMat_];
    double pdc_sum[sizMat_][sizMat_];
    double pdc_mnz[sizMat_][sizMat_];
    double norPdc_sum[sizMat_];//for normalize pdc_sum
    double norPdc_mnz[sizMat_];//for normalize pdc_mnz
    double FCdq_sum[sizMat_]; //F_cluster(d;q) based on CombSUM
    double FCdq_mnz[sizMat_];//F_cluster(d;q) based on CombMNZ
    double sumF_sum=0.0;
    double sumF_mnz=0.0;
    double sumcFdq_sum=0.0;
    double sumcFdq_mnz=0.0;
    int ind=0;
    double sumpcqpdc_sum=0.0;
    double sumpcqpdc_mnz=0.0;
    
    //W=new double*[sizMat_];
    //pdc_sum=new double*[sizMat_];
    //pdc_mnz=new double*[sizMat_];
    //cInf=new double*[sizMat_];
    //pdq_sum=new double[sizMat_];
    //pdq_mnz=new double[sizMat_];
    //pcq_sum=new double[sizMat_];
    //pcq_mnz=new double[sizMat_];
    //norPdc_sum=new double[sizMat_];
    //norPdc_mnz=new double[sizMat_];
    //FCdq_sum=new double[sizMat_];
    //FCdq_mnz=new double[sizMat_];
    //thr=new double[sizMat_];//threshold weights for the delta nearest neighbors
    //Fdq_sum=new double[sizMat_];
    //Fdq_mnz=new double[sizMat_];
    //for(int i=0; i<sizMat_; i++)
    //{
        //W[i]=new double[sizMat_];
        //cInf[i]=new double[sizMat_];
        //pdc_sum[i]=new double[sizMat_];
        //pdc_mnz[i]=new double[sizMat_];
    //}
    
    for(int i=0; i<sizMat_; i++ )
    {
        for(int j=0; j<sizMat_; j++)
        {
            if(i==j)
            {
                W[i][j]=1.0;
            }
            else
            {
                W[i][j]=this->W_[i][j];//W is for cluster W_ is computed by manifold
            }
        }
    }//for(int i=0; i<sizMat_; i++ )
    it_sum=this->qDS_sum_.find(queStr);
    it_mnz=this->qDS_mnz_.find(queStr);
    ind=0;
    for(it_dS_sum=it_sum->second.begin(); it_dS_sum!=it_sum->second.end(); it_dS_sum++)
    {
        sumF_sum=sumF_sum+it_dS_sum->second;
        Fdq_sum[ind]=it_dS_sum->second;
        ind=ind+1;
    }
    ind=0;
    for(it_dS_mnz=it_mnz->second.begin(); it_dS_mnz!=it_mnz->second.end(); it_dS_mnz++)
    {
        sumF_mnz=sumF_mnz+it_dS_mnz->second;
        Fdq_mnz[ind]=it_dS_mnz->second;
        ind=ind+1;
    }
    ind=0;
    for(it_dS_sum=it_sum->second.begin(); it_dS_sum!=it_sum->second.end(); it_dS_sum++)
    {
        //dS_pdq_sum.insert(lss::DOCSCO_PAIR_T(it_dS_sum->first, it_dS_sum->second/sumF_sum));
        pdq_sum[ind]=it_dS_sum->second/sumF_sum;
        ind=ind+1;
    }
    ind=0;
    for(it_dS_mnz=it_mnz->second.begin(); it_dS_mnz!=it_mnz->second.end(); it_dS_mnz++)
    {
        //dS_pdq_mnz.insert(lss::DOCSCO_PAIR_T(it_dS_mnz->first, it_dS_mnz->second/sumF_mnz));
        pdq_mnz[ind]=it_dS_mnz->second/sumF_mnz;
        ind=ind+1;
    }
    for(int i=0; i<sizMat_; i++)
    {
        std::vector<double> douVec;
        for(int j=0; j<sizMat_; j++)
        {
            douVec.push_back(W[i][j]);
        }//for(int j=0; j<sizMat_; j++)
        std::sort(douVec.rbegin(), douVec.rend());
        if(this->delta_<sizMat_ && this->delta_>0)
        {
            thr[i]=douVec[this->delta_-1];
        }
        else
        {
            thr[i]=douVec[sizMat_-1];
        }
    }//for(int i=0; i<sizMat_; i++)
    for(int i=0; i<sizMat_; i++)
    {
        for(int j=0; j<sizMat_; j++)
        {
            if(W[i][j]>=thr[i])
            {
                cInf[i][j]=1.0;//the corresponding documents are in the cluster
            }
            else
            {
                cInf[i][j]=0.0;//the corresponding documents are not in the cluster
            }
        }
    }
    for(int i=0; i<sizMat_; i++)
    {
        pcq_sum[i]=1.0;
        pcq_mnz[i]=1.0;
        for(int j=0; j<sizMat_; j++)
        {
            if(cInf[i][j]>=1.0)
            {
                pcq_sum[i]=pcq_sum[i]*Fdq_sum[j];
                pcq_mnz[i]=pcq_mnz[i]*Fdq_mnz[j];
            }
        }
    }
    sumcFdq_sum=0.0;
    sumcFdq_mnz=0.0;
    for(int i=0; i<sizMat_; i++)
    {
        sumcFdq_sum=sumcFdq_sum+pcq_sum[i];
        sumcFdq_mnz=sumcFdq_mnz+pcq_mnz[i];
    }
    for(int i=0; i<sizMat_; i++)
    {
        pcq_sum[i]=pcq_sum[i]/sumcFdq_sum;
        pcq_mnz[i]=pcq_mnz[i]/sumcFdq_mnz;
    }
    
    for(int i=0; i<sizMat_; i++)
    {
        for(int j=0; j<sizMat_; j++)
        {
            pdc_sum[i][j]=0.0;
            pdc_mnz[i][j]=0.0;
            for(int k=0; k<sizMat_; k++)
            {
                if(cInf[i][k]>=1.0)//d' $\in$ c
                {
                    pdc_sum[i][j]=pdc_sum[i][j]+W[j][k];
                    pdc_mnz[i][j]=pdc_mnz[i][j]+W[j][k];
                }
            }
        }//for(int j=0; j<sizMat_; j++)
    }//for(int i=0; i<sizMat_; i++)
    for(int i=0; i<sizMat_; i++)
    {
        norPdc_sum[i]=0.0;
        norPdc_mnz[i]=0.0;
        for(int j=0; j<sizMat_; j++)
        {
            norPdc_sum[i]=norPdc_sum[i]+pdc_sum[i][j];
            norPdc_mnz[i]=norPdc_mnz[i]+pdc_mnz[i][j];
        }
    }
    for(int i=0; i<sizMat_; i++)
    {
        for(int j=0; j<sizMat_; j++)
        {
            pdc_sum[i][j]=pdc_sum[i][j]/norPdc_sum[i];
            pdc_mnz[i][j]=pdc_mnz[i][j]/norPdc_mnz[i];
        }
    }
    for(int i=0; i<sizMat_; i++)
    {
        FCdq_sum[i]=0.0;
        FCdq_mnz[i]=0.0;
        sumpcqpdc_sum=0.0;
        sumpcqpdc_mnz=0.0;
        for(int j=0; j<sizMat_; j++)
        {
            sumpcqpdc_sum=sumpcqpdc_sum+pcq_sum[j]*pdc_sum[j][i];
            sumpcqpdc_mnz=sumpcqpdc_mnz+pcq_mnz[j]*pdc_mnz[j][i];
        }
        FCdq_sum[i]=(1.0-lambda_)*pdq_sum[i]+lambda_*sumpcqpdc_sum;
        FCdq_mnz[i]=(1.0-lambda_)*pdq_mnz[i]+lambda_*sumpcqpdc_mnz;
    }
    ind=0;
    for(it_dS_sum=it_sum->second.begin(); it_dS_sum!=it_sum->second.end(); it_dS_sum++)
    {
        dS_sum.insert(lss::DOCSCO_PAIR_T(it_dS_sum->first, FCdq_sum[ind]));
        dS_mnz.insert(lss::DOCSCO_PAIR_T(it_dS_sum->first, FCdq_mnz[ind]));
        ind=ind+1;
    }
    it_clu_sum=this->qDS_clu_sum_.find(queStr);
    if(it_clu_sum==this->qDS_clu_sum_.end())
    {
        this->qDS_clu_sum_.insert(lss::QUEDOCSCO_PAIR_T(queStr, dS_sum));
    }
    it_clu_mnz=this->qDS_clu_mnz_.find(queStr);
    if(it_clu_mnz==this->qDS_clu_mnz_.end())
    {
        this->qDS_clu_mnz_.insert(lss::QUEDOCSCO_PAIR_T(queStr, dS_mnz));
    }
    
    /*for(int i=0; i<sizMat_; i++)
    {
        if(W[i]!=NULL)
        {
            delete[] W[i];
            W[i]=NULL;
        }
        if(cInf[i]!=NULL)
        {
            delete[] cInf[i];
            cInf[i]=NULL;
        }
        if(pdc_sum[i]!=NULL)
        {
            delete[] pdc_sum[i];
            pdc_sum[i]=NULL;
        }
        if(pdc_mnz[i]!=NULL)
        {
            delete[] pdc_mnz[i];
            pdc_mnz[i]=NULL;
        }
    }
    delete[] W;
    W=NULL;
    delete[] cInf;
    cInf=NULL;
    delete[] pdc_sum;
    pdc_sum=NULL;
    delete[] pdc_mnz;
    pdc_mnz=NULL;
    if(pdq_sum!=NULL)
    {
        delete[] pdq_sum;
        pdq_sum=NULL;
    }
    if(pdq_mnz!=NULL)
    {
        delete[] pdq_mnz;
        pdq_mnz=NULL;
    }
    if(pcq_sum!=NULL)
    {
        delete[] pcq_sum;
        pcq_sum=NULL;
    }
    if(pcq_mnz!=NULL)
    {
        delete[] pcq_mnz;
        pcq_mnz=NULL;
    }
    if(thr!=NULL)
    {
        delete[] thr;
        thr=NULL;
    }
    if(Fdq_sum!=NULL)
    {
        delete[] Fdq_sum;
        Fdq_sum=NULL;
    }
    if(Fdq_mnz!=NULL)
    {
        delete[] Fdq_mnz;
        Fdq_mnz=NULL;
    }
    if(norPdc_sum!=NULL)
    {
        delete[] norPdc_sum;
        norPdc_sum=NULL;
    }
    if(norPdc_mnz!=NULL)
    {
        delete[] norPdc_mnz;
        norPdc_mnz=NULL;
    }
    if(FCdq_sum!=NULL)
    {
        delete[] FCdq_sum;
        FCdq_sum=NULL;
    }
    if(FCdq_mnz!=NULL)
    {
        delete[] FCdq_mnz;
        FCdq_mnz=NULL;
    }*/
}

lss::QUEDOCSCO_T
lss::ManifoldCluster::getQDSManSUM()
{
    return this->qDS_man_sum_;
}

lss::QUEDOCSCO_T
lss::ManifoldCluster::getQDSManMNZ()
{
    return this->qDS_man_mnz_;
}

lss::QUEDOCSCO_T
lss::ManifoldCluster::getQDSCluSUM()
{
    return this->qDS_clu_sum_;
}

lss::QUEDOCSCO_T
lss::ManifoldCluster::getQDSCluMNZ()
{
    return this->qDS_clu_mnz_;
}

void
lss::ManifoldCluster::wriEvaResult()
{
    std::cout<<"Start evaluating manifold based and cluster based fusion results... ..."<<std::endl;
    lss::ResultWriter* resWri=new lss::ResultWriter();
    std::string savFil=lemur::api::ParamGetString("manifoldCombSUM", "manSUM");
    resWri->writeResult(qDS_man_sum_, savFil);
    savFil=lemur::api::ParamGetString("manifoldCombMNZ", "manMNZ");
    resWri->writeResult(qDS_man_mnz_, savFil);
    savFil=lemur::api::ParamGetString("clusterCombSUM", "cluSUM");
    resWri->writeResult(qDS_clu_sum_, savFil);
    savFil=lemur::api::ParamGetString("clusterCombMNZ", "cluMNZ");
    resWri->writeResult(qDS_clu_mnz_, savFil);
    delete resWri;
    resWri=NULL;
    std::cout<<"Finish the experiment successfully."<<std::endl;
}

void
lss::ManifoldCluster::deleteW_()
{
    for(int i=0; i<sizMat_; i++)
    {
        if(W_[i]!=NULL)
        {
            delete[] W_[i];
            W_[i]=NULL;
        }
    }//for(int i=0; i<sizMat; i++)
    delete[] W_;
    W_=NULL;
}


