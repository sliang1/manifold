//
//  ModelTimeSet.cpp
//  latentfuse
//
//  Created by Shangsong Liang on 12/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "ModelTimeSet.h"

lss::ModelTimeSet::ModelTimeSet(lss::Runs runs, lss::Burst bur)
{
    numOfRuns_=runs.getNumOfRuns();
    int weiTyp=1;
    int maxIteTim=0;
    double numOfAsp=0.0;
    double lamOne=0.0;
    double lamTwo=0.0;
    double leaRat=0.0;
    double docNumInRun=0.0;
    double alpha=0.0;
    lemur::api::ParamGet("weightingType", weiTyp, 1);
    weiTyp_=weiTyp;
    lemur::api::ParamGet("numOfAsp", numOfAsp, 30);
    numOfAsp_=numOfAsp;
    lemur::api::ParamGet("lamOne", lamOne, 0.00001);
    lamOne_=lamOne;
    lemur::api::ParamGet("lamTwo", lamTwo, 0.00001);
    lamTwo_=lamTwo;
    lemur::api::ParamGet("learningRate", leaRat, 0.03);
    leaRat_=leaRat;
    lemur::api::ParamGet("maxIterationTime", maxIteTim, 1000);
    maxIteTim_=maxIteTim;
    lemur::api::ParamGet("docNumInRun", docNumInRun, 30);
    docNumInRun_=docNumInRun;
    lemur::api::ParamGet("alpha2", alpha, 1);
    alpha_=alpha;
    std::cout<<"docNumInRun="<<docNumInRun_<<std::endl;
    this->iniQAllDocInd(runs);
    this->computQueDocScoTimSet(runs, bur);
    this->wriEvaResult();
}

void
lss::ModelTimeSet::iniQAllDocInd(lss::Runs& runs)
{
    const int numOfRuns=runs.getNumOfRuns();
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itDS;
    lss::DOCSCO_T::iterator itAllDocInd;
    double index=0.0;
    
    for(int i=0; i<numOfRuns; i++)
    {
        for(itQDS=runs.qDS[i].begin(); itQDS!=runs.qDS[i].end(); itQDS++)
        {
            itQAllDocInd=qAllDocInd_.find(itQDS->first);
            if(itQAllDocInd==qAllDocInd_.end())
            {
                lss::DOCSCO_T docInd;
                for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
                {
                    docInd.insert(lss::DOCSCO_PAIR_T(itDS->first, 0.0));
                }
                qAllDocInd_.insert(lss::QUEDOCSCO_PAIR_T(itQDS->first, docInd));
            }
            else
            {
                for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
                {
                    itAllDocInd=itQAllDocInd->second.find(itDS->first);
                    if(itAllDocInd==itQAllDocInd->second.end())
                    {
                        itQAllDocInd->second.insert(lss::DOCSCO_PAIR_T(itDS->first, 0.0));
                    }
                }
            }
        }//for(itQDS=runs.qDS[i].begin(); 
    }//for(int i=0; i<numOfRuns;
    for(itQDS=qAllDocInd_.begin(); itQDS!=qAllDocInd_.end(); itQDS++)
    {
        index=0.0;
        for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
        {
            itDS->second=index;
            index=index+1;
        }
    }
}

void
lss::ModelTimeSet::computQueDocScoTimSet(lss::Runs& runs, lss::Burst& bur)
{
    lss::QUEDOCSCO_T::iterator itQDS;
    lss::DOCSCO_T::iterator itDS;
    lss::QUEDOCSCO_T::iterator itQAllDocInd;
    lss::DOCSCO_T::iterator itAllDocInd;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;// qTimIndDocVec;
    lss::DOCINDDOC_T::iterator itDocIndDoc;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    const int numOfLis=numOfRuns_;
    const int numOfRun=numOfRuns_;
    const int numOfAsp=numOfAsp_;
    const int weiTyp=weiTyp_;
    const double lamOne=lamOne_;
    const double lamTwo=lamTwo_;
    const double leaRat=leaRat_;
    const double alpha=alpha_;
    double cost=0.0;
    double sv=0.0;
    //double Iij=0.0;
    double sumn[numOfAsp];
    double summ[numOfAsp];
    double s[numOfAsp][numOfLis];
    int index=0;
    int timInd=0; //time index
    bool hasRead=false;

    for(itQAllDocInd=qAllDocInd_.begin(); itQAllDocInd!=qAllDocInd_.end(); itQAllDocInd++)
    {
        lss::DOCINDDOC_T docIndDoc;
        lss::DOCSCO_T docSco;
        const int numOfDoc=itQAllDocInd->second.size();
        double r[numOfLis][numOfDoc];
        double Iij[numOfLis][numOfDoc]; //indicator
        double v[numOfDoc][numOfAsp];
        double svLis; //sv sum of lists
        index=0;
        itQTimIndDocVec=bur.qTimIndDocVec.find(itQAllDocInd->first);
        if(itQTimIndDocVec==bur.qTimIndDocVec.end())
        {
            std::cout<<"There is an error in qTimIndDocVec in the class: Burst."<<std::endl;
            return;
        }
        //std::cout<<"query="<<itQAllDocInd->first<<std::endl;
        for(itAllDocInd=itQAllDocInd->second.begin(); itAllDocInd!=itQAllDocInd->second.end(); itAllDocInd++)
        {
            docIndDoc.insert(lss::DOCINDDOC_PAIR_T(index, itAllDocInd->first));
            for(int i=0; i<numOfLis; i++)
            {
                itQDS=runs.qDS[i].find(itQAllDocInd->first);
                if(itQDS==runs.qDS[i].end())
                {
                    std::cout<<"There is an error in computing qDS in Runs class."<<std::endl;
                    continue;
                }
                itDS=itQDS->second.find(itAllDocInd->first);
                if(itDS==itQDS->second.end())
                {
                    r[i][index]=0.0;
                    Iij[i][index]=0.0;
                }
                else
                {
                    r[i][index]=(docNumInRun_-itDS->second)/docNumInRun_;
                    if(weiTyp==0)
                    {
                        Iij[i][index]=1.0;
                    }
                    else if(weiTyp==1)
                    {
                        Iij[i][index]=1.0/(itDS->second+1);
                    }
                    else if(weiTyp==2)
                    {
                        Iij[i][index]=1.0/(log2(itDS->second+1.0+1.0));
                    }
                    else if(weiTyp==3)
                    {
                        Iij[i][index]=1.0/pow(2.0, itDS->second);
                    }
                    //std::cout<<runs.getFilNamVec()[i]<<" "<<itQDS->first<<" "<<itDS->first<<" "<<itDS->second<<std::endl;
                }
            }//for(int i=0; i<numOfLis; i++)
            //std::cout<<itQAllDocInd->first<<" "<<itAllDocInd->first<<" "<<index<<std::endl;
            index=index+1;
        }//for(itAllDocInd=itQAllDocInd->second.begin(); initialize r
        for(int a=0; a<numOfAsp; a++)
        {
            for(int i=0; i<numOfLis; i++)
            {
                s[a][i]=0.5;
            }
        }//initialize s
        for(int j=0; j<numOfDoc; j++)
        {
            for(int a=0; a<numOfAsp; a++)
            {
                v[j][a]=0.5;
            }
        }//initialize v
        for(int ite=0; ite<maxIteTim_; ite++)
        {
            cost=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    cost=cost+Iij[i][j]*pow(r[i][j]-sv, 2.0);
                }
            }
            //std::cout<<"error="<<cost<<std::endl;
            for(int i=0; i<numOfLis; i++) //update s_i
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=0.0;
                }
                for(int j=0; j<numOfDoc; j++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++) //s_i^T * v_j
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sumn[a]=sumn[a]+Iij[i][j]*(sv-r[i][j])*v[j][a];
                    }
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    sumn[a]=sumn[a]+lamOne*s[a][i];
                }
                for(int a=0; a<numOfAsp; a++)
                {
                    s[a][i]=s[a][i]-leaRat*sumn[a]*s[a][i];
                }
            }//for(int i=0; i<numOfLis; i++), for S_i
            
            for(int j=0; j<numOfDoc; j++) //update v_j
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=0.0;
                }
                for(int i=0; i<numOfLis; i++)
                {
                    sv=0.0;
                    for(int a=0; a<numOfAsp; a++)
                    {
                        sv=sv+s[a][i]*v[j][a];
                    }
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+Iij[i][j]*(sv-r[i][j])*s[a][i];
                    }
                }
                itDocIndDoc=docIndDoc.find(j);
                itDocTimInd=bur.docTimInd.find(itDocIndDoc->second);
                itTimIndDocVec=itQTimIndDocVec->second.find(itDocTimInd->second);
                if(itTimIndDocVec->second.size()>1)
                {
                    for(int a=0; a<numOfAsp; a++)
                    {
                        summ[a]=summ[a]+alpha*v[j][a];
                        for(int k=0; k<itTimIndDocVec->second.size(); k++)
                        {
                            summ[a]=summ[a]-alpha*1.0/itTimIndDocVec->second.size()*v[k][a];
                        }
                    }
                }
                //if(itTimIndDocVec->second.size()==1)
                //{
                  //  std::cout<<"++++++++++"<<std::endl;
                //}
                for(int a=0; a<numOfAsp; a++)
                {
                    summ[a]=summ[a]+lamTwo*v[j][a];
                    v[j][a]=v[j][a]-leaRat*summ[a]*v[j][a];
                }
            } //for(int j=0; j<numOfDoc; j++)
        }//for(int ite=0; ite<maxIteTim_; ite++)
        for(int j=0; j<numOfDoc; j++)
        {
            svLis=0.0;
            for(int i=0; i<numOfLis; i++)
            {
                for(int a=0; a<numOfAsp; a++)
                {
                    svLis=svLis+Iij[i][j]*s[a][i]*v[j][a];
                }
            }
            itDocIndDoc=docIndDoc.find(j);
            docSco.insert(lss::DOCSCO_PAIR_T(itDocIndDoc->second, svLis));
        }
        qDSco_.insert(lss::QUEDOCSCO_PAIR_T(itQAllDocInd->first, docSco));
    }//for(itQAllDocInd=qAllDocInd_.begin();
    
    /*
    for(itQDS=runs.qDS[1].begin(); itQDS!=runs.qDS[1].end(); itQDS++)
    {
        //std::cout<<" query="<<itQDS->first<<std::endl;
        if(itQDS->first=="MB001")
        {
            for(itDS=itQDS->second.begin(); itDS!=itQDS->second.end(); itDS++)
            {
                std::cout<<"*"<<itDS->first<<std::endl;
            }
        }
        count=count+itQDS->second.size();
    }
    */
}

void
lss::ModelTimeSet::wriEvaResult()
{
    lss::ResultWriter resWri;
    std::string savFil=lemur::api::ParamGetString("timeSetComb", "timeSet");
    resWri.writeResult(qDSco_, savFil);
}






