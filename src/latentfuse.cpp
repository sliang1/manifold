//
//  fuse.cpp
//  Fusion
//
//  Created by Shangsong Liang on 9/16/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//  sliangshangsssss

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>

//#include "CombSUM.h"
//#include "StaInf.h"
#include "Runs.h"
#include "CombSUMMNZ.h"
#include "Distance.h"
#include "ManifoldCluster.h"
#include "ResultWriter.h"
#include "Burst.h"
#include "ModelTimeSet.h"
#include "ModelBurst.h"

void usage() {
    std::cerr<<std::endl << "USAGE: .//fuse configure-file" << std::endl
    << std::endl
    << "  Command description:" << std::endl
    << "  --------------------" << std::endl
    << "  The defaul parameter will select and merge the ranked listed randomly." << std::endl
    << "  -m fixed -t value means select and merge the top best ranked lists. Here VALUE is the number of ranked lists which are used." << std::endl
    <<"   -m fixed -b value means select and merge the bottom worst ranked lists. Here VALUE is the number of ranked lists which are used."
    << std::endl
    << std::endl;
}

int main (int argc, const char * argv[])
{
    try {
        if(lemur::api::ParamPushFile(argv[1]))
        {
            std::cout<<"Import parameters for running successfully."<<std::endl;
        }
        else
        {
            std::cout<<"Can not import parameters."<<std::endl;
            usage();
            return 0;
        }
    } catch (const std::exception& e) {
        usage();
        std::cout<<e.what()<<std::endl;
    }
    lss::Runs runs=lss::Runs();
    lss::CombSUMMNZ combSUMMNZ=lss::CombSUMMNZ(runs);
    std::clock_t start, end;
    start=std::clock();
    lss::Burst burst=lss::Burst(combSUMMNZ.getQDSSUM());
    //lss::ModelTimeSet modTimSet=lss::ModelTimeSet(runs, burst);
    lss::ModelBurst modBurst=lss::ModelBurst(runs, burst, combSUMMNZ);
    end=std::clock();
    std::cout<<"The execution for TimeRA is:"<<std::endl;
    std::cout<<(double)(end-start)/(double)CLOCKS_PER_SEC<<std::endl;
    //lss::Distance* distance=new lss::Distance(*combSUMMNZ);
    //lss::ManifoldCluster manClu=lss::ManifoldCluster(combSUMMNZ);
    
    
    //delete runs;
    //runs=NULL;
    //delete combSUMMNZ;
    //combSUMMNZ=NULL;
    //delete distance;
    //delete manClu;
    //manClu=NULL;
}









