//
//  Burst.cpp
//  latentfuse
//
//  Created by Shangsong Liang on 12/15/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "Burst.h"
#include "DataTypesTwo.h"

lss::Burst::Burst(lss::QUEDOCSCO_T qDFScore)
{
    const std::string timFile=lemur::api::ParamGetString("timFile");
    //std::cout<<"timFile="<<timFile<<std::endl;
    this->initDocTimePairs(timFile);
    queInt=this->wriQueInterval(qDFScore);
    this->wriQueTimIndDocVec(qDFScore);
    this->wriQueDocInBur(qDFScore);
    this->wriQueDocDocSco(qDFScore);
}

void
lss::Burst::initDocTimePairs(const std::string& timFile)
{
    std::fstream input;
    std::string str;
    std::string docID;
    std::string substr;
    int day=0;
    int hour=0;
    int timInd=0;
    const int starDay=23;//NOTE: the start day in the Tweet collection is from 23 January
    unsigned long first=0;
    unsigned long last=0;
    std::stringstream ss;
    lss::TIME_T time;
    lss::DOCTIME_T::iterator it;
    
    input.open(timFile.c_str(), std::ios::in);
    if(!input)
    {
        std::cout<<"Can not open the file: "<<timFile<<std::endl;
        return;
    }
    
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        first=str.find(' ', 0);
        last=str.rfind(' ', str.length()-1);
        docID=str.substr(0, first);
        substr=str.substr(first+1, last-first-1);
        ss<<substr;
        ss>>day;
        ss.clear();
        substr=str.substr(last+1, str.length()-last-1);
        ss<<substr;
        ss>>hour;
        ss.clear();
        time.day=day;
        time.hour=hour;
        it=docTim.find(docID);
        if(it==docTim.end())
        {
            docTim.insert(lss::DOCTIME_PAIR_T(docID, time));
            if(time.day>=starDay)//belong to January
            {
                timInd=(time.day-starDay)*24+time.hour+1;
            }
            else//belong to Feburary
            {
                timInd=(31-starDay+1)*24+(time.day-1)*24+time.hour+1;
            }
            docTimInd.insert(lss::DOCTIMEIND_PAIR_T(docID,timInd));
            //std::cout<<docID<<" "<<time.day<<" "<<time.hour<<" "<<timInd<<std::endl;
        }        
    }//while(input.eof()!=true)
    input.close();
}

lss::QUEINTERVAL_T
lss::Burst::wriQueInterval(lss::QUEDOCSCO_T qDFScore)
{
    //lss::QUEDOCFEATURES_T::iterator it;
    int maxQueNum=0;
    lss::DOCTIME_T::iterator itTim;
    lss::DOCTIME_T::reverse_iterator revItTim;
    //const int timLen=0;
    //lss::DOCFEATURES_T::iterator itDoc;
    lss::QUEDOCSCO_T::iterator it_qDFScore;
    lss::DOCSCO_T::iterator it_dFScore;
    int docTimInd=0;//document time index
    int starDay=1;
    lss::QUEINTERVAL_T queInt;
    
    itTim=docTim.begin();
    revItTim=docTim.rbegin();
    starDay=itTim->second.day;
    starDay_=starDay;
    //std::cout<<"The starting day is: "<<itTim->second.day<<" hour is: "<<itTim->second.hour<<std::endl;
    //std::cout<<"The ending day is: "<<revItTim->second.day<<" hour is: "<<revItTim->second.hour<<std::endl;
    const int timLen=(31-itTim->second.day+1)*24+(revItTim->second.day-1)*24+revItTim->second.hour+1;
    //std::cout<<"timeLen="<<timLen<<std::endl;
    for(it_qDFScore=qDFScore.begin(); it_qDFScore!=qDFScore.end(); it_qDFScore++)//for each query
    {
        double* docFre=new double[timLen]();
        std::string qID="";
        int indSta=0;
        int indEnd=0;
        int nonzeroNum=0;
        lss::TIMEINTERVAL_T timInt;
        std::vector<TIMEINTERVAL_T> timIntVec;
        std::vector<TIMEINTERVAL_T> timIntVec_cutoff;
        
        for(it_dFScore=it_qDFScore->second.begin(); it_dFScore!=it_qDFScore->second.end(); it_dFScore++)
        {
            itTim=docTim.find(it_dFScore->first);
            if(itTim==docTim.end())
            {
                std::cout<<"There is an error when initialing the member docTim."<<std::endl;
                std::cout<<"it_dFScore->first="<<it_dFScore->first<<std::endl;
                delete docFre;
                return queInt;
            }
            if(itTim->second.day>=starDay)//belong to January
            {
                docTimInd=(itTim->second.day-starDay)*24+itTim->second.hour+1;
            }
            else//belong to Febuary
            {
                docTimInd=(31-starDay+1)*24+(itTim->second.day-1)*24+itTim->second.hour+1;
            }
            docFre[docTimInd]=docFre[docTimInd]+it_dFScore->second;
        }//for(it_dFScore=it_qDFScore->second.begin();
        
        for(int i=0; i<timLen; i++)
        {
            if(docFre[i]>0)
            {
                indSta=i;
                break;
            }
        }
        for(int i=timLen-1; i>=0; i--)
        {
            if(docFre[i]>0)
            {
                indEnd=i;
                break;
            }
        }
        for(int k=0; k<timLen; k++)
        {
            if(docFre[k]>0.0)
            {
                nonzeroNum=nonzeroNum+1;
            }
        }
        const int INSIZE=indEnd-indSta+1;
        //const int INSIZE=nonzeroNum;
        //std::cout<<"INSIZE="<<INSIZE<<std::endl;
        //std::cout<<"nonzero="<<nonzeroNum<<std::endl;
        double* docFre2=new double[INSIZE];
        double sumDocFre=0.0;
        for(int i=0; i<INSIZE; i++)
        {
            docFre2[i]=docFre[i+indSta];
            sumDocFre=sumDocFre+docFre[i+indSta];
        }
        for(int i=0; i<INSIZE; i++)
        {
            ///docFre2[i]=docFre2[i]/sumDocFre-1.0/INSIZE;
            //docFre2[i]=docFre2[i]/sumDocFre-1.0/INSIZE;
            //std::cout<<"**="<<docFre2[i]<<std::endl;
            docFre2[i]=docFre2[i]/sumDocFre-1.0/nonzeroNum;
        }
        const double* NONE=0;
        const lss::PType UNUSED=std::make_pair(NONE, NONE);
        lss::PType* output=new lss::PType[INSIZE/2+1];
        const int PSZ=INSIZE/2+1;
        for(int i=0; i<PSZ; ++i)
        {
            output[i]=UNUSED;
        }
        //mss::AlgMSS(docFre2, docFre2+INSIZE, output, 0.0);
        mss::AlgMSS(docFre2, docFre2+INSIZE, output, 0.0);
        
        timIntVec.clear();
        for(int i=0; i<PSZ; ++i)
        {
            if(output[i]==UNUSED)
            {
                break;
            }
            timInt.score=0.0;
            for(const double* j=output[i].first; j!=output[i].second; j++)
            {
                timInt.score=timInt.score+(*j);
            }
            timInt.from=output[i].first-docFre2+indSta;
            timInt.to=output[i].second-docFre2-1+indSta;
            timIntVec.push_back(timInt);
            /*
            if(it_qDFScore->first=="MB010")
            {
                std::cout<<"timInt.from="<<timInt.from<<"  timInt.to="<<timInt.to<<std::endl;
            }
             */
        }
        for(int i=(int)timIntVec.size()-1; i>=0; i--)
        {
            for(int j=0; j<i; j++)
            {
                if(timIntVec[j].score<timIntVec[j+1].score)
                {
                    timInt.score=timIntVec[j].score;
                    timInt.from=timIntVec[j].from;
                    timInt.to=timIntVec[j].to;
                    timIntVec[j].score=timIntVec[j+1].score;
                    timIntVec[j].from=timIntVec[j+1].from;
                    timIntVec[j].to=timIntVec[j+1].to;
                    timIntVec[j+1].score=timInt.score;
                    timIntVec[j+1].from=timInt.from;
                    timIntVec[j+1].to=timInt.to;
                }
            }
        }//for(int i=(int)timIntVec.size()-1; i>=0; i--)
        /*
         timIntVec_cutoff.clear();
         for(int k=0; k<3 && k<timIntVec.size(); k++)
         {
         timIntVec_cutoff.push_back(timIntVec[k]);
         }
         queInt.insert(lss::QUEINTERVAL_PAIR_T(it->first, timIntVec_cutoff));
         */
        queInt.insert(lss::QUEINTERVAL_PAIR_T(it_qDFScore->first, timIntVec));
        if(docFre!=NULL)
        {
            delete docFre;
        }
        if(docFre2!=NULL)
        {
            delete docFre2;
        }
        if(NONE!=NULL)
        {
            delete NONE;
        }
        if(output!=NULL)
        {
            delete output;
        }
    }//it_qDFScore=qDFScore.begin();
    lss::QUEINTERVAL_T::iterator itQueInt;
    std::vector<TIMEINTERVAL_T> timIntVec;
    TIMEINTERVAL_T temp;
    int i=0;
    /*
    for(itQueInt=queInt.begin(); itQueInt!=queInt.end(); itQueInt++)
    {
        timIntVec.clear();
        timIntVec=itQueInt->second;
        for(i=0; i!=itQueInt->second.size();i++)
        {
            std::cout<<itQueInt->first<<" "<<itQueInt->second[i].from<<" "<<itQueInt->second[i].to<<std::endl;
        }
    }*/
    return queInt;
}

void
lss::Burst::wriQueTimIndDocVec(lss::QUEDOCSCO_T& qDFScore)
{
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::QUEDOCSCO_T::iterator itQDFScore;
    lss::DOCSCO_T::iterator itDFScore;
    lss::QUETIMINDDOCVEC_T::iterator itQTimIndDocVec;
    lss::TIMINDDOCVEC_T::iterator itTimIndDocVec;
    int ind;
    
    for(itQDFScore=qDFScore.begin(); itQDFScore!=qDFScore.end(); itQDFScore++)
    {
        itQTimIndDocVec=qTimIndDocVec.find(itQDFScore->first);
        if(itQTimIndDocVec==qTimIndDocVec.end())
        {
            lss::TIMINDDOCVEC_T timIndDocVec;
            for(itDFScore=itQDFScore->second.begin(); itDFScore!=itQDFScore->second.end(); itDFScore++)
            {
                itDocTimInd=docTimInd.find(itDFScore->first);
                if(itDocTimInd==docTimInd.end())
                {
                    std::cout<<"There is an error when construct the docTimInd."<<std::endl;
                    return;
                }
                ind=itDocTimInd->second;
                itTimIndDocVec=timIndDocVec.find(ind);
                if(itTimIndDocVec==timIndDocVec.end())
                {
                    std::vector<std::string> strVec;
                    strVec.push_back(itDFScore->first);
                    timIndDocVec.insert(lss::TIMINDDOCVEC_PAIR_T(ind, strVec));
                }
                else
                {
                    itTimIndDocVec->second.push_back(itDFScore->first);
                }
                //timIndDocVec.insert(lss::TIMINDDOCVEC_PAIR_T());
                //std::cout<<itQDFScore->first<<" "<<itDFScore->first<<" "<<ind<<std::endl;
            }
            qTimIndDocVec.insert(lss::QUETIMINDDOCVEC_PAIR_T(itQDFScore->first, timIndDocVec));
            //std::cout<<itQDFScore->first<<std::endl;
        }
    }//for(itQDFScore=qDFScore.begin();
    
    /*for(itQTimIndDocVec=qTimIndDocVec.begin(); itQTimIndDocVec!=qTimIndDocVec.end(); itQTimIndDocVec++)
    {
        for(itTimIndDocVec=itQTimIndDocVec->second.begin(); itTimIndDocVec!=itQTimIndDocVec->second.end(); itTimIndDocVec++)
        {
            std::cout<<itQTimIndDocVec->first<<" "<<itTimIndDocVec->first<<" "<<itTimIndDocVec->second.size()<<std::endl;
        }
    }*/
}

void
lss::Burst::wriQueDocInBur(lss::QUEDOCSCO_T& qDFScore)
{
    lss::QUEINTERVAL_T::iterator itQueInt;
    lss::QUEDOCSCO_T::iterator itQDFscore;
    lss::DOCTIMEIND_T::iterator itDocTimInd;
    lss::DOCSCO_T::iterator itDocSco;
    int ind;
    
    for(itQDFscore=qDFScore.begin(); itQDFscore!=qDFScore.end(); itQDFscore++)
    {
        std::vector<std::string> docVec;
        itQueInt=queInt.find(itQDFscore->first);
        if(itQueInt==queInt.end())
        {
            std::cout<<"There is an error in computing queInt."<<std::endl;
            return;
        }
        for(itDocSco=itQDFscore->second.begin(); itDocSco!=itQDFscore->second.end(); itDocSco++)
        {
            itDocTimInd=docTimInd.find(itDocSco->first);
            if(itDocTimInd==docTimInd.end())
            {
                std::cout<<"There is an error in computing docTimInd."<<std::endl;
                return;
            }
            ind=itDocTimInd->second;
            for(int i=0; i<itQueInt->second.size(); i++)
            {
                if((itQueInt->second[i].from<=ind) && (itQueInt->second[i].to>=ind))
                {
                    docVec.push_back(itDocTimInd->first);
                    break;
                }
            }
        }//for(itDocSco=itQDFscore->second.begin();
        qDocInBur.insert(lss::QUEDOCINBUR_PAIR_T(itQDFscore->first, docVec));
    }//for(itQDFscore=qDFScore.begin();
    
    /*
    lss::QUEDOCINBUR_T::iterator itQDocInBur;
    for(itQDocInBur=qDocInBur.begin(); itQDocInBur!=qDocInBur.end(); itQDocInBur++)
    {
        for(int i=0; i<itQDocInBur->second.size(); i++)
        {
            std::cout<<itQDocInBur->first<<" "<<itQDocInBur->second[i]<<std::endl;
        }
    }
     */
    /*
    for(itQueInt=queInt.begin(); itQueInt!=queInt.end(); itQueInt++)
    {
        for(int i=0; i!=itQueInt->second.size();i++)
        {
            std::cout<<itQueInt->first<<" "<<itQueInt->second[i].from<<" "<<itQueInt->second[i].to<<std::endl;
        }
    }
     */
}


void
lss::Burst::wriQueDocDocSco(lss::QUEDOCSCO_T& qDFScore)
{
    lss::QUEDOCSCO_T::iterator itQDFScore;
    lss::DOCSCO_T::iterator itDoc;
    lss::QUEDOCINBUR_T::iterator itQDocInBur;
    lss::DOCTIMEIND_T::iterator itDocTimInd1;
    lss::DOCTIMEIND_T::iterator itDoctimInd2;
    lss::QUEINTERVAL_T::iterator itQueInt;
    int ind1=0;
    int ind2=0;
    double n_sigma=0.0;
    double from_sigma=0.0;
    double to_sigma=0.0;
    double dis=0.0;//distance
    double sigma=0.0;
    
    for(itQDFScore=qDFScore.begin(); itQDFScore!=qDFScore.end(); itQDFScore++)
    {
        lss::DOCDOCSCO_T docDocSco;
        itQDocInBur=qDocInBur.find(itQDFScore->first);
        if(itQDocInBur==qDocInBur.end())
        {
            std::cout<<"There is an error in qDocInBur."<<std::endl;
            return;
        }
        itQueInt=queInt.find(itQDFScore->first);
        for(itDoc=itQDFScore->second.begin(); itDoc!=itQDFScore->second.end(); itDoc++)
        {
            itDocTimInd1=docTimInd.find(itDoc->first);
            ind1=itDocTimInd1->second;
            lss::DOCSCO_T docSco;
            for(int i=0; i<itQDocInBur->second.size(); i++)
            {
                itDoctimInd2=docTimInd.find(itQDocInBur->second[i]);
                ind2=itDoctimInd2->second;
                for(int j=0; j<itQueInt->second.size(); j++)
                {
                    if((itQueInt->second[j].from<=ind2) && (itQueInt->second[j].to>=ind2))
                    {
                        from_sigma=itQueInt->second[j].from;
                        to_sigma=itQueInt->second[j].to;
                        break;
                    }
                }
                n_sigma=to_sigma-from_sigma+1;
                sigma=(pow(n_sigma,2)-1)/12;
                //sigma=(pow(408*1.5,2.0)-1.0)/6.0+1.0;
                dis=exp(-( pow(ind1-ind2,2) )/(4*sigma+1.0));
                //////////////////////***************************
                if(dis<0.00001)
                {
                    dis=0.0;
                }
                docSco.insert(lss::DOCSCO_PAIR_T(itQDocInBur->second[i],dis));
                //docDocSco.insert(lss::DOCDOCSCO_PAIR_T(itDoc->first));
            }//for(int i=0; i<itQDocInBur->second.size();
            docDocSco.insert(lss::DOCDOCSCO_PAIR_T(itDoc->first, docSco));
        }//for(itDoc=itQDFScore->second.begin(); 
        qDocDocSco.insert(lss::QUEDOCDOCSCO_PAIR_T(itQDFScore->first, docDocSco));
    }//for(itQDFScore=qDFScore.begin();
    
    /*
    lss::QUEDOCDOCSCO_T::iterator itQDocDocSco;
    lss::DOCDOCSCO_T::iterator itDocDocSco;
    for(itQDocDocSco=qDocDocSco.begin(); itQDocDocSco!=qDocDocSco.end(); itQDocDocSco++)
    {
        if(itQDocDocSco->first=="MB050")
        {
            for(itDocDocSco=itQDocDocSco->second.begin(); itDocDocSco!=itQDocDocSco->second.end(); itDocDocSco++)
            {
                for(itDoc=itDocDocSco->second.begin(); itDoc!=itDocDocSco->second.end(); itDoc++)
                {
                    std::cout<<itQDocDocSco->first<<" "<<itDocDocSco->first<<" "<<itDoc->first<<" "<<itDoc->second<<std::endl;
                }
            }
        }//if
    }//for(itQDocDocSco=qDocDocSco.begin();
     */
}
















