//
//  CombSUM.cpp
//  Fusion
//
//  Created by Shangsong Liang on 9/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "CombSUMMNZ.h"
#include "ResultWriter.h"

lss::CombSUMMNZ::CombSUMMNZ(const lss::Runs& runs)
{
    this->start_=std::clock();
    this->wriQDSSUMMNZ(runs);
    this->wriEvaResult();
    this->finish_=std::clock();
    std::cout<<"The execution for both CombSUM and CombMNZ is: "<<std::endl;
    std::cout<<(this->finish_-this->start_)/double(CLOCKS_PER_SEC)<<" s."<<std::endl;
}

void
lss::CombSUMMNZ::wriQDSSUMMNZ(lss::Runs runs)
{
    lss::QUEDOCSCO_T::iterator it_qDS;
    lss::QUEDOCSCO_T::iterator it_qDS_temp;
    lss::QUEDOCSCO_T::iterator it_qDS_sum;//for CombSUM
    lss::QUEDOCSCO_T::iterator it_qDS_mnz;//for CombMNZ
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSCO_T::iterator it_dS_temp;
    lss::DOCSCO_T::iterator it_dS_sum;//for CombSUM
    lss::DOCSCO_T::iterator it_dS_mnz;//for CombMNZ
    int numOfRuns=runs.getNumOfRuns();
    int docNumInRun=runs.getDocNumInRun();
    lss::QUEDOCSCO_T qDSTemp; //temporal value of qDS for writing to qDSSUM_ and qDSMNZ_
    double resTmp=0.0;
    int timOfRunsAppIn=0;//times of runs the document appeals in the runs
    std::vector<std::string> filNamVec=runs.getFilNamVec();
    std::clock_t start_CombSUM;
    std::clock_t end_CombSUM;
    std::clock_t start_CombMNZ;
    std::clock_t end_CombMNZ;
    
    for(int i=0; i<numOfRuns; i++)
    {
        for(it_qDS=runs.qDS[i].begin(); it_qDS!=runs.qDS[i].end(); it_qDS++)
        {
            it_qDS_temp=qDSTemp.find(it_qDS->first);
            if(it_qDS_temp==qDSTemp.end())
            {
                lss::DOCSCO_T dS_temp;
                for(it_dS=it_qDS->second.begin(); it_dS!=it_qDS->second.end(); it_dS++)
                {
                    dS_temp.insert(lss::DOCSCO_PAIR_T(it_dS->first, 0.0));
                }
                qDSTemp.insert(lss::QUEDOCSCO_PAIR_T(it_qDS->first, dS_temp));
            }
            else
            {
                for(it_dS=it_qDS->second.begin(); it_dS!=it_qDS->second.end(); it_dS++)
                {
                    it_qDS_temp->second.insert(lss::DOCSCO_PAIR_T(it_dS->first, 0.0));
                }
            }
        }//for(it_qDS=runs.qDS[i].begin(); 
    }//for(int i=0; i<numOfRuns; i++)
    
    this->qDSSUM_=qDSTemp;
    this->qDSMNZ_=qDSTemp;
    
    for(it_qDS_temp=qDSTemp.begin(); it_qDS_temp!=qDSTemp.end(); it_qDS_temp++)
    {
        //start_CombMNZ=std::clock();
        for(it_dS_temp=it_qDS_temp->second.begin(); it_dS_temp!=it_qDS_temp->second.end(); it_dS_temp++)
        {
            resTmp=0.0;
            timOfRunsAppIn=0;
            for(int i=0; i<runs.getNumOfRuns(); i++)
            {
                it_qDS=runs.qDS[i].find(it_qDS_temp->first);
                if(it_qDS==runs.qDS[i].end())
                {
                    continue;
                }
                else
                {
                    it_dS=it_qDS->second.find(it_dS_temp->first);
                    if(it_dS==it_qDS->second.end())
                    {
                        continue;
                    }
                    else
                    {
                        resTmp=resTmp+(docNumInRun-it_dS->second)/(docNumInRun*1.0);
                        timOfRunsAppIn=timOfRunsAppIn+1;
                    }
                }
            }//for(int i=0; i<runs.getNumOfRuns(); i++)
            it_qDS_sum=qDSSUM_.find(it_qDS_temp->first);
            if(it_qDS_sum==qDSSUM_.end())
            {
                std::cout<<"There is an error in initializing qDSSUM_."<<std::endl;
                return;
            }
            it_dS_sum=it_qDS_sum->second.find(it_dS_temp->first);
            if(it_dS_sum==it_qDS_sum->second.end())
            {
                std::cout<<"There is an error in initializing qDSSUM_.."<<std::endl;
                return;
            }
            it_dS_sum->second=resTmp;
            
            it_qDS_mnz=qDSMNZ_.find(it_qDS_temp->first);
            if(it_qDS_mnz==qDSMNZ_.end())
            {
                std::cout<<"There is an error in initializing qDSMNZ_."<<std::endl;
                return;
            }
            it_dS_mnz=it_qDS_mnz->second.find(it_dS_temp->first);
            if(it_dS_mnz==it_qDS_mnz->second.end())
            {
                std::cout<<"There is an error in initializing qDSMNZ_.."<<std::endl;
                return;
            }
            it_dS_mnz->second=timOfRunsAppIn*resTmp;
            //std::cout<<it_qDS_sum->first<<" "<<it_dS_sum->first<<" "<<it_dS_sum->second<<" times="<<timOfRunsAppIn<<" "<<it_dS_mnz->second<<std::endl;
        }//for(it_dS_temp=it_qDS_temp->second.begin(); 
        //end_CombMNZ=std::clock();
        //std::cout<<it_qDS_temp->first<<" "<<(double)(start_CombMNZ-end_CombMNZ)/(double)CLOCKS_PER_SEC<<std::endl;
    }//for(it_qDS_temp=qDSTemp.begin();
}

lss::QUEDOCSCO_T
lss::CombSUMMNZ::getQDSSUM()
{
    return qDSSUM_;
}

lss::QUEDOCSCO_T
lss::CombSUMMNZ::getQDSMNZ()
{
    return qDSMNZ_;
}

lss::QUEDOCSCO_T
lss::CombSUMMNZ::getQTDSSUM()
{
    return qTDSSUM_;
}

lss::QUEDOCSCO_T
lss::CombSUMMNZ::getQTDSMNZ()
{
    return qTDSMNZ_;
}

void
lss::CombSUMMNZ::wriEvaResult()
{
    lss::ResultWriter* resWri=new lss::ResultWriter();
    double percent;
    lemur::api::ParamGet("percentTopDoc", percent, 0.2);
    std::string savFil=lemur::api::ParamGetString("combSUM", "combSUM");
    resWri->writeResult(qDSSUM_, savFil, qTDSSUM_, percent);
    savFil=lemur::api::ParamGetString("combMNZ", "combMNZ");
    resWri->writeResult(qDSMNZ_, savFil, qTDSMNZ_, percent);
    delete resWri;
}


