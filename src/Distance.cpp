//
//  Distance.cpp
//  Fusion
//
//  Created by Shangsong Liang on 9/19/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "Distance.h"

// Lemur
#include "Param.hpp"
#include "LemurIndriIndex.hpp"
#include "IndexManager.hpp"
#include "DocumentRep.hpp"
#include "DocUnigramCounter.hpp"
#include "UnigramLM.hpp"

#include "SimpleKLRetMethod.hpp"
#include "common_headers.hpp"
#include "DocUnigramCounter.hpp"
#include "RelDocUnigramCounter.hpp"
#include "OneStepMarkovChain.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <omp.h>

lss::Distance::Distance(lss::CombSUMMNZ combSUMMNZ)
{
    this->initDist(combSUMMNZ);//initialize this->dDS and this->queInd
    //this->initDDS(combSUMMNZ);
    this->initDDS2(combSUMMNZ);
}

void
lss::Distance::initDist(lss::CombSUMMNZ& combSUMMNZ)
{
    lss::QUEDOCSCO_T qDS=combSUMMNZ.getQDSSUM();
    lss::QUEDOCSCO_T::iterator it_qDS;
    lss::QUEIND_T::iterator it_QI;
    int ind=0;
    
    //this->dDS=new lss::DOCDOCSCO_T[qDS.size()];
    this->dDS=new lss::DOCDOCSCO_T();
    
    for(it_qDS=qDS.begin(); it_qDS!=qDS.end(); it_qDS++)
    {
        it_QI=this->queInd.find(it_qDS->first);
        if(it_QI==this->queInd.end())
        {
            this->queInd.insert(QUEIND_PAIR_T(it_qDS->first, ind));
            ind=ind+1;
        }
    }//for(it_qDS=qDS.begin(); it_qDS!=qDS.end(); it_qDS++)
    this->numOfQue_=ind;
    this->smoMet_=lemur::api::ParamGetString("smoothing");
    double smoMet=0.0;
    int intTemp=lemur::api::ParamGet("smoothingPar", smoMet);
    this->smoPar_=smoMet;
}

void
lss::Distance::initDDS(lss::CombSUMMNZ& combSUMMNZ)
{
    
}

void
lss::Distance::initDDS2(lss::CombSUMMNZ& combSUMMNZ)
{
    this->openIndex();
    this->writDDS(combSUMMNZ);
    this->closeIndex();
}

void
lss::Distance::openIndex()
{
    std::string indFil=lemur::api::ParamGetString("index");
    if(indFil.length()<=0)
    {
        std::cout<<"Parameter 'index' is not set."<<std::endl;
        return;
    }
    try 
    {
        index_=lemur::api::IndexManager::openIndex(indFil);
    } 
    catch (const lemur::api::Exception& e)
    {
        std::cout<<"Can't open index, check parameter 'index'. "<<std::endl;
    }
}

void
lss::Distance::closeIndex()
{
    if(index_!=NULL)
    {
        delete index_;
    }
}

void
lss::Distance::writDDS(lss::CombSUMMNZ& combSUMMNZ)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::QUEDOCSCO_T::iterator it2;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSCO_T::iterator it_dS2;
    lss::DOCCHA_T::iterator it_dC;
    lss::DOCCHA_T::iterator it_dC2;
    lss::DOCDOCSCO_T::iterator it_dDS;
    double skLScor=0.0;
    lss::QUEDOCSCO_T qDS=combSUMMNZ.getQDSSUM();
    lss::QUEDOCSCO_T qDS2=combSUMMNZ.getQDSMNZ();
    std::cout<<combSUMMNZ.getQDSSUM().size()<<std::endl;
    int ind=0;
    for(it=qDS.begin(); it!=qDS.end(); it++)
    {
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            it_dC=this->docCha_.find(it_dS->first);
            if(it_dC==this->docCha_.end())
            {
                this->docCha_.insert(lss::DOCCHA_PAIR_T(it_dS->first, ' '));
            }
        }
    }
    
    for(it_dC=this->docCha_.begin(); it_dC!=this->docCha_.end(); it_dC++)
    {
        it_dC2=it_dC;
        it_dC2++;
        for(; it_dC2!=this->docCha_.end(); it_dC2++)
        {
            skLScor=this->computeKLScor(it_dC->first, it_dC2->first);
            it_dDS=dDS->find(it_dC->first);
            if(it_dDS==dDS->end())
            {
                lss::DOCSCO_T dS;
                dS.insert(lss::DOCSCO_PAIR_T(it_dC2->first, skLScor));
                dDS->insert(lss::DOCDOCSCO_PAIR_T(it_dC->first, dS));
                std::cout<<"doc="<<it_dC->first<<" doc="<<it_dC2->first<<" sKL="<<skLScor<<std::endl;
            }
            else
            {
                it_dDS->second.insert(lss::DOCSCO_PAIR_T(it_dC2->first, skLScor));
                std::cout<<"doc="<<it_dC->first<<" doc="<<it_dC2->first<<" sKL="<<skLScor<<std::endl;
            }
        }//for(; it_dC2!=this->docCha_.end(); it_dC2++)
    }
}

double
lss::Distance::computeKLScor(std::string doc1, std::string doc2)
{
    int docID1=index_->document(doc1);
    int docID2=index_->document(doc2);
    double docLen1=index_->docLength(docID1);
    double docLen2=index_->docLength(docID2);
    double skLScor=0.0;//symmetric K-L divergence score

    lemur::langmod::DocUnigramCounter* collCounter1 
    = new lemur::langmod::DocUnigramCounter(docID1,*index_ );
    lemur::langmod::DocUnigramCounter* collCounter2
    = new lemur::langmod::DocUnigramCounter(docID2, *index_);
    lemur::langmod::DocUnigramCounter* collCounter
    =new lemur::langmod::DocUnigramCounter(*index_);
    
    lemur::langmod::UnigramLM *collModel1 
    = new lemur::langmod::MLUnigramLM( *collCounter1, index_->termLexiconID() );
    lemur::langmod::UnigramLM *collModel2
    = new lemur::langmod::MLUnigramLM( *collCounter2, index_->termLexiconID() );
    lemur::langmod::UnigramLM *collModel
    = new lemur::langmod::MLUnigramLM( *collCounter, index_->termLexiconID() );
    
    int termID=0;
    double prob=0.0;
    double probSum=0.0;
    double smoPro1=0.0;//smoothing p(w|Q)
    double smoPro2=0.0;//smoothing p(w|D)
    double muPwC=0.0;//mu*p(w|c)
    double sumWInQ=0.0;//term in Q
    double sumWInDNotInQ=0.0;//term in D but not in Q
    double sumWOutQAndD=0.0;// term in the collection, but not in both Q and D (outside both Q and D)
    std::string termStr;
    collModel1->startIteration();
    for(;collModel1->hasMore();)
    {
        collModel1->nextWordProb(termID, prob);
        muPwC=this->smoPar_*collModel->prob(termID);
        if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
        {
            smoPro1=(prob*docLen1+muPwC)/(docLen1+this->smoPar_);
            smoPro2=(collModel2->prob(termID)*docLen2+muPwC)/(docLen2+this->smoPar_);
            sumWInQ=sumWInQ+smoPro1*(log(smoPro1/smoPro2));
        }
    }
    skLScor=sumWInQ;
    collModel2->startIteration();
    for(;collModel2->hasMore();)
    {
        collModel2->nextWordProb(termID, prob);
        muPwC=this->smoPar_*collModel->prob(termID);
        if(this->smoMet_=="dirichlet"||this->smoMet_=="Dirichlet")
        {
            smoPro1=(collModel1->prob(termID)*docLen1+muPwC)/(docLen1+this->smoPar_);
            smoPro2=(prob*docLen2+muPwC)/(docLen2+this->smoPar_);
            sumWInDNotInQ=sumWInDNotInQ+smoPro2*log(smoPro2/smoPro1);
        }
    }
    skLScor=skLScor+sumWInDNotInQ;
    
    delete collCounter1;
    delete collCounter2;
    delete collModel1;
    delete collModel2;
    return 0.5*skLScor;
}


lss::QUEIND_T
lss::Distance::getQueInd()
{
    return this->queInd;
}





