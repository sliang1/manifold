//
//  Runs.cpp
//  Fusion
//
//  Created by Shangsong Liang on 9/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "Runs.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <sstream>
#include <stdlib.h>
#include <vector>

lss::Runs::Runs()
{
    numOfRuns_=0;
    runsPath_=lemur::api::ParamGetString("runs");
    int docNumInRun=0;
    if(runsPath_.length()==0)
    {
        std::cout<<"There is an error in the parameter file, no <runs></runs> parameter."<<std::endl;
        return;
    }
    docNumInRun_=lemur::api::ParamGet("docNumInRun", docNumInRun, 100);//default number of document in each run to be fused is set to be 100
    docNumInRun_=docNumInRun;
    this->initRuns();
    lss::QUEDOCSCO_T::iterator it_qDS;
    lss::DOCSCO_T::iterator it_dS;
}

void
lss::Runs::initRuns()
{
    const std::string temFilStr=TEMPFILE1;
    std::string comStr="ls "+runsPath_;
    std::fstream input;
    std::fstream inputFil;
    std::string filStr="";
    std::string str="";
    std::string str2="";
    std::string queID="";
    std::string docID="";
    double rank=0;
    std::string ranStr="";
    unsigned long first=0;
    unsigned long second=0;
    int mark=0;
    std::stringstream ss;
    int indFil=0;
    lss::QUEDOCSCO_T::iterator it_qDS;
    lss::DOCSCO_T::iterator it_dS;
    bool isFroZer=true;
    bool isFroZer2=false;
    int couLin=0;//count number of document in each run couLin<=docNumInRun
    
    comStr=comStr+">"+TEMPFILE1;
    std::system(comStr.c_str());

    input.open(temFilStr.c_str(), std::ios::in);
    if(!input)
    {
        std::cout<<"Can not open the automatically generated file: "<<temFilStr<<std::endl;
        return;
    }
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        filNamVec.push_back(str);
        numOfRuns_=numOfRuns_+1;
    }
    input.close();
    qDS=new QUEDOCSCO_T[numOfRuns_]; //e.g. qDS[0] belongs to the 0-th run.
    
    input.open(temFilStr.c_str(), std::ios::in);
    if(!input)
    {
        std::cout<<"Can not open the automatically generated file: "<<temFilStr<<std::endl;
        return;
    }
    while(input.eof()!=true)
    {
        std::getline(input, str);
        if(str.length()<=0)
        {
            continue;
        }
        filStr=runsPath_+"/"+str;
        inputFil.open(filStr.c_str(), std::ios::in);
        if(!inputFil)
        {
            std::cout<<"Can not open the file: "<<filStr<<std::endl;
            return;
        }
        couLin=0;
        while(inputFil.eof()!=true)
        {
            std::getline(inputFil, str2);
            if(str2.length()<=0)
            {
                continue;
            }
            first=0;
            mark=0;
            for(unsigned long i=0; i<str2.length(); i++)
            {
                if(str2[i]==' ')
                {
                    second=i;
                    switch (mark) {
                        case 0:{queID=str2.substr(first, second); first=second+1; mark=mark+1; break;}
                        case 1:{first=second+1; mark=mark+1; break;}
                        case 2:{docID=str2.substr(first, second-first);first=second+1; mark=mark+1; break;}
                        case 3:{ranStr=str2.substr(first, second-first);ss.clear();ss<<ranStr; ss>>rank; first=second+1; mark=mark+1; break;}
                    }
                    
                }
            }//for(unsigned long i=0; i<str2.length(); i++)
            //std::cout<<queID<<" Q0 "<<docID<<" "<<rank<<std::endl;
            it_qDS=qDS[indFil].find(queID);
            if(it_qDS==qDS[indFil].end())
            {
                lss::QUEDOCSCO_T tempQueDocSco;
                lss::DOCSCO_T tempDocSco;
                if(rank==0)
                {
                    isFroZer=true;
                }
                else
                {
                    isFroZer=false;
                }
                if(isFroZer==true && rank<docNumInRun_)
                {
                    tempDocSco.insert(lss::DOCSCO_PAIR_T(docID, rank*1.0));
                    qDS[indFil].insert(lss::QUEDOCSCO_PAIR_T(queID, tempDocSco));
                }
                else
                {
                    if(isFroZer==false && rank<=docNumInRun_)
                    {
                        tempDocSco.insert(lss::DOCSCO_PAIR_T(docID, rank*1.0-1.0));
                        qDS[indFil].insert(lss::QUEDOCSCO_PAIR_T(queID, tempDocSco));
                    }
                }
            }//if(it_qDS==qDS[indFil].end())
            else
            {
                it_dS=it_qDS->second.find(docID);
                if(it_dS==it_qDS->second.end())
                {
                    if(isFroZer==true && rank<docNumInRun_)
                    {
                        it_qDS->second.insert(lss::DOCSCO_PAIR_T(docID, rank*1.0));
                    }
                    else
                    {
                        if(isFroZer==false && rank<=docNumInRun_)
                        {
                            it_qDS->second.insert(lss::DOCSCO_PAIR_T(docID, rank*1.0-1.0));
                        }
                    } 
                }
            }
            couLin=couLin+1;
        }//while(inputFil.eof()!=true && couLin<docNumInRun_)
        inputFil.close();
        indFil=indFil+1;
    }//while(input.eof()!=true)
    comStr="rm "+temFilStr;
    std::system(comStr.c_str());
    input.close();
}

int
lss::Runs::getNumOfRuns()
{
    return this->numOfRuns_;
}


std::vector<std::string>
lss::Runs::getFilNamVec()
{
    return this->filNamVec;
}

int
lss::Runs::getDocNumInRun()
{
    return docNumInRun_;
}

