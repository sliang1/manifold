//
//  ResultWriter.cpp
//  Fusion
//
//  Created by Shangsong Liang on 9/25/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#include "ResultWriter.h"
#include "DataTypesTwo.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <utility>


// Lemur
#include "Param.hpp"

void
lss::ResultWriter::writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSTRSCO_T dS_str;//a document-score struct
    lss::DOCSTRSCO_T strTemp; //a temporal struct
    std::string savFil;
    std::fstream outputFil;
    std::string comStr="";//command string for writing evaluation file
    std::string trecEvalParStr=TRECPARMETER;
    std::string savEvaFil;
    int rank=0;
    
    savPath_=lemur::api::ParamGetString("resultPath", "result");//default path is 'result'
    savFil=savPath_+"/"+filNam;
    qrelFil_=lemur::api::ParamGetString("qrelGT");
    if(qrelFil_.length()<=0)
    {
        std::cout<<"Can not read the qrel file path, please check the parameter file."<<std::endl;
        return;
    }
    trecEvalFil_=lemur::api::ParamGetString("trecEval");
    if(trecEvalFil_.length()<=0)
    {
        std::cout<<"Can not read the trec_eval file path, please check the parameter file."<<std::endl;
        return;
    }
    outputFil.open(savFil.c_str(), std::ios::out);
    if(!outputFil)
    {
        std::cout<<"Can not write to the file: "<<savFil<<". please check the parameter file."<<std::endl;
        return;
    }
    for(it=qDS.begin(); it!=qDS.end(); it++)
    {
        std::vector<lss::DOCSTRSCO_T> dS_vec;
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            dS_str.docStr=it_dS->first;
            dS_str.sco=it_dS->second;
            dS_vec.push_back(dS_str);
        }
        for(long int i=dS_vec.size()-1; i>=0; i--)//sorting
        {
            for(long int j=0; j<i; j++)
            {
                if(dS_vec[j].sco<dS_vec[j+1].sco)
                {
                    strTemp.docStr=dS_vec[j].docStr;
                    strTemp.sco=dS_vec[j].sco;
                    dS_vec[j].docStr=dS_vec[j+1].docStr;
                    dS_vec[j].sco=dS_vec[j+1].sco;
                    dS_vec[j+1].docStr=strTemp.docStr;
                    dS_vec[j+1].sco=strTemp.sco;
                }
            }
        }//for(long int i=dS_vec.size()-1; i>=0; i--)
        for(int i=0; i<dS_vec.size(); i++)
        {
            rank=i+1;
            outputFil<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
            //std::cout<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
        }
    }//for(it=qDS.begin(); it!=qDS.end(); it++)
    outputFil.close();
    savEvaFil=savFil+".eva";
    comStr=trecEvalFil_+" "+trecEvalParStr+" "+qrelFil_+" "+savFil+" >"+savEvaFil;
    std::system(comStr.c_str());
}

void
lss::ResultWriter::writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, lss::QUEDOCSCO_T& qTopDS, const double percent)
{
    lss::QUEDOCSCO_T::iterator it;
    lss::QUEDOCSCO_T::iterator itQTopDS;
    lss::DOCSCO_T::iterator it_dS;
    lss::DOCSTRSCO_T dS_str;//a document-score struct
    lss::DOCSTRSCO_T strTemp; //a temporal struct
    std::string savFil;
    std::fstream outputFil;
    std::string comStr="";//command string for writing evaluation file
    std::string trecEvalParStr=TRECPARMETER;
    std::string savEvaFil;
    int rank=0;
    
    savPath_=lemur::api::ParamGetString("resultPath", "result");//default path is 'result'
    savFil=savPath_+"/"+filNam;
    qrelFil_=lemur::api::ParamGetString("qrelGT");
    if(qrelFil_.length()<=0)
    {
        std::cout<<"Can not read the qrel file path, please check the parameter file."<<std::endl;
        return;
    }
    trecEvalFil_=lemur::api::ParamGetString("trecEval");
    if(trecEvalFil_.length()<=0)
    {
        std::cout<<"Can not read the trec_eval file path, please check the parameter file."<<std::endl;
        return;
    }
    outputFil.open(savFil.c_str(), std::ios::out);
    if(!outputFil)
    {
        std::cout<<"Can not write to the file: "<<savFil<<". please check the parameter file."<<std::endl;
        return;
    }
    for(it=qDS.begin(); it!=qDS.end(); it++)
    {
        std::vector<lss::DOCSTRSCO_T> dS_vec;
        lss::DOCSCO_T dS;
        for(it_dS=it->second.begin(); it_dS!=it->second.end(); it_dS++)
        {
            dS_str.docStr=it_dS->first;
            dS_str.sco=it_dS->second;
            dS_vec.push_back(dS_str);
        }
        for(long int i=dS_vec.size()-1; i>=0; i--)//sorting
        {
            for(long int j=0; j<i; j++)
            {
                if(dS_vec[j].sco<dS_vec[j+1].sco)
                {
                    strTemp.docStr=dS_vec[j].docStr;
                    strTemp.sco=dS_vec[j].sco;
                    dS_vec[j].docStr=dS_vec[j+1].docStr;
                    dS_vec[j].sco=dS_vec[j+1].sco;
                    dS_vec[j+1].docStr=strTemp.docStr;
                    dS_vec[j+1].sco=strTemp.sco;
                }
            }
        }//for(long int i=dS_vec.size()-1; i>=0; i--)
        for(int i=0; i<dS_vec.size(); i++)
        {
            rank=i+1;
            outputFil<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
            //std::cout<<it->first<<" Q0 "<<dS_vec[i].docStr<<" "<<rank<<" "<<dS_vec[i].sco<<" "<<filNam<<std::endl;
        }
        for(int i=0; i<dS_vec.size() && i<=percent*dS_vec.size(); i++)
        {
            dS.insert(DOCSCO_PAIR_T(dS_vec[i].docStr, dS_vec[i].sco));
        }
        qTopDS.insert(QUEDOCSCO_PAIR_T(it->first, dS));
    }//for(it=qDS.begin(); it!=qDS.end(); it++)
    outputFil.close();
    savEvaFil=savFil+".eva";
    comStr=trecEvalFil_+" "+trecEvalParStr+" "+qrelFil_+" "+savFil+" >"+savEvaFil;
    std::system(comStr.c_str());
}




