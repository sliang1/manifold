//
//  ModelTimeSet.h
//  latentfuse
//
//  Created by Shangsong Liang on 12/17/12.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//
#ifndef _MODELTIMESET_H
#define _MODELTIMESET_H

#include "DataTypesTwo.h"
#include "Burst.h"
#include "Runs.h"
#include "ResultWriter.h"

//Lemur
#include "Param.hpp"

namespace lss
{
    class ModelTimeSet
    {
    public:
        ModelTimeSet(lss::Runs runs, lss::Burst bur);
    private:
        lss::QUEDOCSCO_T qDSco_;//final query-document-score
        lss::QUEDOCSCO_T qAllDocInd_; //query-document(any document appear in any lists to be fused)-index
        int numOfRuns_;//number of runs being used to be fused
        int numOfAsp_;
        double lamOne_; //lambda one for S
        double lamTwo_; //lambda two for V
        double leaRat_; //learning rate
        double docNumInRun_;
        double alpha_;
        int weiTyp_; // the weighting type for Iij 1: if document j exists in the i-th list, Iij=1, otherwise Iij=0; 2: if document j exists in the i-th list, Iij=1/(log_2 (rank+1+1)), otherwise Iij=0, rank is from 0 (for top-1 document)
        int maxIteTim_; //maximum iteration times
        void computQueDocScoTimSet(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model time set
        void iniQAllDocInd(lss::Runs& runs);
        void wriEvaResult();
    };
}


#endif