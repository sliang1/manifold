//
//  Burst.h
//  latentfuse
//
//  Created by Shangsong Liang on 12/15/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//
#ifndef _BURST_H
#define _BURST_H

#include  "DataTypesTwo.h"
#include  "MSS.h"
#include  <math.h>

// Lemur
#include "Param.hpp"

namespace lss 
{
    class Burst
    {
    public:
        Burst(lss::QUEDOCSCO_T qDFScore);
        lss::DOCTIME_T docTim;
        lss::DOCTIMEIND_T docTimInd; //document - time index
        lss::QUEINTERVAL_T wriQueInterval(lss::QUEDOCSCO_T qDFScore);
        lss::QUEINTERVAL_T queInt;
        lss::QUETIMINDDOCVEC_T qTimIndDocVec;
        lss::QUEDOCDOCSCO_T qDocDocSco;
    private:
        void initDocTimePairs(const std::string& timFile);
        void wriQueTimIndDocVec(lss::QUEDOCSCO_T& qDFScore);
        void wriQueDocInBur(lss::QUEDOCSCO_T& qDFScore);
        void wriQueDocDocSco(lss::QUEDOCSCO_T& qDFScore);
        int starDay_;
        lss::QUEDOCINBUR_T qDocInBur;//query-documents(in bursts)
    };
}


#endif