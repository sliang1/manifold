//
//  ResultWriter.h
//  Fusion
//
//  Created by Shangsong Liang on 9/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#ifndef _RESULTWRITER_H
#define _RESULTWRITER_H

#include "DataTypesTwo.h"

#include <string>
#include <vector>

namespace lss
{
    class ResultWriter
    {
    public:
        void writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam);
        void writeResult(lss::QUEDOCSCO_T qDS, const std::string filNam, lss::QUEDOCSCO_T& qTopDS, const double percent); // write result and also get (percent) top documents.
    private:
        std::string savPath_;
        std::string qrelFil_;
        std::string trecEvalFil_;
    };
}

#endif