//
//  DataTypesTwo.h
//  Fusion
//
//  Created by Shangsong Liang on 9/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//

#ifndef _DATATYPES_H
#define _DATATYPES_H

#include <map>
#include <string>
#include <utility>
#include <list>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

namespace lss
{
    typedef std::map<std::string, double> DOCSCO_T;
    typedef std::pair<std::string, double> DOCSCO_PAIR_T;
    typedef std::map<std::string, DOCSCO_T> QUEDOCSCO_T;//query-document-score(also for rank)
    typedef std::pair<std::string, DOCSCO_T> QUEDOCSCO_PAIR_T;
    
    typedef std::map<std::string, DOCSCO_T> DOCDOCSCO_T;//document-document-score(KL distance score)
    typedef std::pair<std::string, DOCSCO_T> DOCDOCSCO_PAIR_T;
    typedef std::map<std::string, int> QUEIND_T;//query-index
    typedef std::pair<std::string, int> QUEIND_PAIR_T;
    
    typedef std::map<std::string, char> DOCCHA_T;
    typedef std::pair<std::string, char> DOCCHA_PAIR_T;
    
    typedef struct{std::string docStr; double sco;} DOCSTRSCO_T;
    
    typedef struct{int day; int hour;} TIME_T;
    typedef std::map<std::string, TIME_T> DOCTIME_T;
    typedef std::pair<std::string, TIME_T> DOCTIME_PAIR_T;
    typedef std::map<std::string, int> DOCTIMEIND_T;
    typedef std::pair<std::string, int> DOCTIMEIND_PAIR_T;
    
    typedef struct{double from; double to; double score;} TIMEINTERVAL_T;//document burst from x to y, score is the score for the interval
    typedef std::map<std::string, std::vector<TIMEINTERVAL_T> > QUEINTERVAL_T;
    typedef std::pair<std::string, std::vector<TIMEINTERVAL_T> > QUEINTERVAL_PAIR_T;
    
    typedef std::pair<const double*, const double*> PType;
    
    typedef std::map<int, std::vector<std::string> > TIMINDDOCVEC_T; //time index - document id string
    typedef std::pair<int, std::vector<std::string> > TIMINDDOCVEC_PAIR_T;
    typedef std::map<std::string, TIMINDDOCVEC_T> QUETIMINDDOCVEC_T;
    typedef std::pair<std::string, TIMINDDOCVEC_T> QUETIMINDDOCVEC_PAIR_T;
    
    typedef std::map<std::string, DOCDOCSCO_T> QUEDOCDOCSCO_T;
    typedef std::pair<std::string, DOCDOCSCO_T> QUEDOCDOCSCO_PAIR_T; //query-doc-doc-score for burst doc-doc score for a given query
    
    typedef std::map<std::string, std::vector<std::string> > QUEDOCINBUR_T; //query-doc(the documents are in the bursts)
    typedef std::pair<std::string, std::vector<std::string> > QUEDOCINBUR_PAIR_T;
    
    typedef std::map<int, std::string> DOCINDDOC_T; //(document index)-document in array
    typedef std::pair<int, std::string> DOCINDDOC_PAIR_T;
    
    typedef std::map<std::string, int> DOCDOCIND_T; // document-(document index) in array
    typedef std::pair<std::string, int> DOCDOCIND_PAIR_T;
    
    typedef struct{int ind; double cov;} INDCOV_T;// document index- coviariance
    
    #define TEMPFILE1 "fileNames.temp"
    #define TEMPFILE2 "fileNames2.temp"
    #define TRECPARMETER " -q -m all_trec "
    #define SMALLVALUE 0.001
}
#endif