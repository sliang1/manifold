//
//  Manifold.h
//  Fusion
//
//  Created by Shangsong Liang on 9/18/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//
#ifndef _MANIFOLDCLUSTER_H
#define _MANIFOLDCLUSTER_H

#include "DataTypesTwo.h"
#include "Runs.h"
#include "CombSUMMNZ.h"
#include <string>
#include <vector>

//Lemur
#include "Index.hpp"

namespace lss
{
    class ManifoldCluster
    {
    public:
        ManifoldCluster(lss::CombSUMMNZ combSUMMNZ);
        ~ManifoldCluster();
        lss::QUEDOCSCO_T getQDSManSUM();//query-document-score for manifold based ranking using CombSUM
        lss::QUEDOCSCO_T getQDSManMNZ();//query-document-score for manifold based ranking using CombMNZ
        lss::QUEDOCSCO_T getQDSCluSUM();//query-document-score for cluster based ranking using CombSUM
        lss::QUEDOCSCO_T getQDSCluMNZ();//query-document-score for cluster based ranking using CombMNZ
    private:
        const lemur::api::Index* index_;
        void openIndex();
        void closeIndex();
        void initSmoMetPar();//initilazing smoothing method and parameter
        void wriEvaResult();
        double computeKLScor(std::string doc1, std::string doc2);
        double computeKLScor2(std::string doc1, std::string doc2);
        std::string smoMet_;//smoothing method, dirichlet, or JM
        double smoPar_;//smoothing parameter
        int tSSDN_;//top standard ranking scores (CombSUM, CombMNZ) Document Number
        double alpha_; // alpha*S*F(t)+(1-alpha)*Y
        int maxInt_;//maximum interation
        int delta_;// the number of documents in a cluster
        double lambda_;
        double aveSquErr_;//average square error
        lss::QUEDOCSCO_T qDS_man_sum_;//query-document-score based on manifold, using combSUM
        lss::QUEDOCSCO_T qDS_man_mnz_;//query-document-score based on manifold, using combMNZ
        lss::QUEDOCSCO_T qDS_clu_sum_;//query-document-score based on cluster, using combSUM
        lss::QUEDOCSCO_T qDS_clu_mnz_;//query-document-score based on cluster, using combMNZ
        lss::QUEDOCSCO_T qDS_sum_;//combSUMMNZ.getQDSSUM();
        lss::QUEDOCSCO_T qDS_mnz_;//combSUMMNZ.getQDSMNZ();
        void wriQDSManClu();//write query-document score for qDS_man_ and qDS_clu_
        void wriWQDSManForSpeQue(std::string queStr);//write query-document score for the weight matrix W_ and qDS_man_ for a special query: queStr
        void wriQDSCluForSpeQue(std::string queStr);//write query-document score for a special query: queStr
        void deleteW_();
        double** W_;
        int sizMat_;
    };
}

#endif