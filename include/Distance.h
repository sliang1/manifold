//
//  Distance.h
//  Fusion
//
//  Created by Shangsong Liang on 9/19/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//
#ifndef _DISTANCE_H
#define _DISTANCE_H

#include "DataTypesTwo.h"
#include "CombSUMMNZ.h"

//Lemur
#include "Index.hpp"

#include <string>
#include <vector>

namespace lss
{
    class Distance
    {
    public:
        Distance(lss::CombSUMMNZ combSUMMNZ);
        lss::DOCDOCSCO_T* dDS; //dDS include document-document KL scores for 0-th query, no query information in dDS
        lss::QUEIND_T getQueInd();
    private:
        lss::QUEIND_T queInd;
        lss::DOCCHA_T docCha_;//save all documents' ID appealing in the runs
        int numOfQue_;//number of query
        void initDist(lss::CombSUMMNZ& combSUMMNZ);
        void initDDS(lss::CombSUMMNZ& combSUMMNZ);//initialize score for dDS, score=0;
        void initDDS2(lss::CombSUMMNZ& combSUMMNZ);
        const lemur::api::Index* index_;
        void openIndex();
        void closeIndex();
        void writDDS(lss::CombSUMMNZ& combSUMMNZ);
        double computeKLScor(std::string doc1, std::string doc2);
        std::string smoMet_;//smoothing method, dirichlet, or JM
        double smoPar_;//smoothing parameter
    };
}

#endif