//
//  ModelBurst.h
//  latentfuse
//
//  Created by Shangsong Liang on 12/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//
#ifndef _MODELBURST_H
#define _MODELBURST_H

#include "DataTypesTwo.h"
#include "Burst.h"
#include "Runs.h"
#include "ResultWriter.h"
#include "CombSUMMNZ.h"

//Lemur
#include "Param.hpp"

namespace lss
{
    class ModelBurst
    {
    public:
        ModelBurst(lss::Runs runs, lss::Burst bur, lss::CombSUMMNZ combSUMMNZ);
    private:
        lss::QUEDOCSCO_T qDSco_;//final query-document-score
        lss::QUEDOCSCO_T qAllDocInd_; //query-document(any document appear in any lists to be fused)-index
        lss::QUEDOCSCO_T qTDSSUM_;
        lss::QUEDOCSCO_T qTDSMNZ_;
        int numOfRuns_;//number of runs being used to be fused
        int numOfAsp_;
        double lamOne_; //lambda one for S
        double lamTwo_; //lambda two for V
        double leaRat_; //learning rate
        double docNumInRun_;
        double alpha_;
        double beta_;
        double bWeight_;
        int weiTyp_; // the weighting type for Iij 1: if document j exists in the i-th list, Iij=1, otherwise Iij=0; 2: if document j exists in the i-th list, Iij=1/(log_2 (rank+1+1)), otherwise Iij=0, rank is from 0 (for top-1 document)
        int maxIteTim_; //maximum iteration times
        void computQueDocScoBurst(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model burst set
        void computQueDocScoBurst2(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model burst set
        void computQueDocScoBurst3(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model 3 burst set
        void computQueDocScoBurst4(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model 4 burst set, with g(x)=S^T V
        void computQueDocScoBurst5(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model 5 burst set, with g(x)=g(S^T V),here g(x)=1.0/(1+exp(-x))
        void computQueDocScoBurst33(lss::Runs& runs, lss::Burst& bur);//compute query-document-score based on model 4 burst set, with g(x)=S^T V
        void iniQAllDocInd(lss::Runs& runs);
        void wriEvaResult();
        inline double gF(double x) {return 1.0/(1.0+exp(-x));}; //g(x)=1.0/(1.0+exp(-x))
        inline double gDerF(double x) {return exp(x)/pow(1.0+exp(x),2.0);}; //g'(x)=exp(x)/(1.0+exp(x))^2
        std::clock_t start_;
        std::clock_t finish_;
    };
}

#endif