//
//  Runs.h
//  Fusion
//
//  Created by Shangsong Liang on 9/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#ifndef _RUNS_H
#define _RUNS_H

#include "DataTypesTwo.h"
#include <string>
#include <vector>

// Lemur
#include "Param.hpp"


namespace lss
{
    class Runs
    {
    public:
        Runs();
        lss::QUEDOCSCO_T* qDS;
        int getNumOfRuns();//number of runs being used to be fused
        int getDocNumInRun();
        std::vector<std::string> getFilNamVec();
    private:
        std::string runsPath_;
        void initRuns();
        int numOfRuns_;//number of runs being used to be fused
        std::vector<std::string> filNamVec;
        int docNumInRun_;//number of document in each run to be fused
    };
}

#endif