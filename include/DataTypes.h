//
//  DataTypes.h
//  tweet
//
//  Created by Shangsong Liang on 2/28/16.
//  Copyright 2016 __UvA__. All rights reserved.
//

#ifndef _DATATYPES_H
#define _DATATYPES_H

#include <map>
#include <string>
#include <utility>
#include <list>
#include <vector>

namespace lss
{
    typedef struct{std::string fn; int rln; double wr; double qu; double ht; double li; double re; double ot;} FEATURES_T;//fn=file name; rln=result lists numbers,how many result lists have this record; wr=weight of result list; qu=query feature value; ht=hashtag feature value; li=link feature value; re=retweets feature value; ot=other feature value;
    typedef std::map<std::string, FEATURES_T> DOCFEATURES_T;
    typedef std::pair<std::string, FEATURES_T> DOCFEATURES_PAIR_T;
    typedef std::map<std::string, DOCFEATURES_T> QUEDOCFEATURES_T;
    typedef std::pair<std::string, DOCFEATURES_T> QUEDOCFEATURES_PAIR_T;
    
    typedef std::map<std::string, double> DOCCOMSUM_T;
    typedef std::pair<std::string, double> DOCCOMSUM_PAIR_T;
    typedef std::map<std::string, DOCCOMSUM_T> QUEDOCCOMSUM_T;
    typedef std::pair<std::string, DOCCOMSUM_T> QUEDOCCOMSUM_PAIR_T;
    
    
    typedef std::map<std::string, double> DOCRELSCO_T;//document-relevant score pair according to ground truth
    typedef std::pair<std::string, double> DOCRELSCO_PAIR_T;
    typedef std::map<std::string, DOCRELSCO_T> QUEDOCRELSCO_T;
    typedef std::pair<std::string, DOCRELSCO_T> QUEDOCRELSCO_PAIR_T;
    
    typedef std::map<std::string, double> DOCDELMAP_T;//document-deltaMAP
    typedef std::pair<std::string, double> DOCDELMAP_PAIR_T;
    typedef std::map<std::string, DOCDELMAP_T> DOCDOCDELMAP_T;//document-document-deltaMAP
    typedef std::pair<std::string, DOCDELMAP_T> DOCDOCDELMAP_PAIR_T;
    typedef struct{std::string doc; double sco; double rel;} DOCSCOREL_T;
    
    typedef std::map<std::string, char> DOCCHA_T;
    typedef std::pair<std::string, char> DOCCHA_PAIR_T;
    
    typedef std::map<std::string, double> LISTWEIGHT_T;
    typedef std::pair<std::string, double> LISTWEIGHT_PAIR_T;
    
    typedef std::map<std::string, double> DOCRETWEETNUM_T;
    typedef std::pair<std::string, double> DOCRETWEETNUM_PAIR_T;
    
    typedef std::map<std::string, double> DOCLINKNUM_T;
    typedef std::pair<std::string, double> DOCLINKNUM_PAIR_T;
    
    typedef std::map<std::string, double> DOCHASNUM_T;
    typedef std::pair<std::string, double> DOCHASNUM_PAIR_T;
    
    typedef std::map<std::string, double> DOCFORANYFEA_T;
    typedef std::pair<std::string, double> DOCFORANYFEA_PAIR_T;
    
    typedef struct{int day; int hour;} TIME_T;
    typedef std::map<std::string, TIME_T> DOCTIME_T;
    typedef std::pair<std::string, TIME_T> DOCTIME_PAIR_T;
    
    typedef struct{double per; int ind;} PERIND_T;//Performance index
    
    typedef struct{double from; double to; double score;} TIMEINTERVAL_T;//document burst from x to y, score is the score for the interval
    typedef std::map<std::string, std::vector<TIMEINTERVAL_T> > QUEINTERVAL_T;
    typedef std::pair<std::string, std::vector<TIMEINTERVAL_T> > QUEINTERVAL_PAIR_T;
    
    typedef std::pair<const double*, const double*> PType;
    
    //query-burst-document-score
    typedef std::map<std::string, double> DOCSCO_T;//document-score
    typedef std::pair<std::string, double> DOCSCO_PAIR_T;
    typedef std::map<std::string, DOCSCO_T> BURDOCSCO_T;//burst-document-score
    typedef std::pair<std::string, DOCSCO_T> BURDOCSCO_PAIR_T;
    typedef std::map<std::string, BURDOCSCO_T> QUEBURDOCSCO_T;//query-burst-document(in the burst or all the documents)-score
    typedef std::pair<std::string, BURDOCSCO_T> QUEBURDOCSCO_PAIR_T;
    
    typedef std::map<std::string, double> BURSCO_T;//burst-score
    typedef std::pair<std::string, double> BURSCO_PAIR_T;
    typedef std::map<std::string, BURSCO_T> QUEBURSCO_T;//query-burst-score
    typedef std::pair<std::string, BURSCO_T> QUEBURSCO_PAIR_T;
    
    typedef std::map<std::string, double> QUESUMF_T;//query-sumF(d,q)
    typedef std::pair<std::string, double> QUESUMF_PAIR_T;
    
    typedef std::map<std::string, double> QUESUMB_T;//query-sumB(C_L)
    typedef std::pair<std::string, double> QUESUMB_PAIR_T;
    
    typedef std::map<std::string, double> QUETIMLEN_T;
    typedef std::pair<std::string, double> QUETIMLEN_PAIR_T;
    
    #define NUMBEROFRANK 30
    #define MAXNUMOFTWEUSED 30
    #define MARKHUNDREDPLUSS 101
    #define TRECPARMETER " -q -c -m all_trec "
    #define TRECPARMETERTRAINING " -q -m all_trec "
    #define TRECPARMETERFORLISTWEI " -m all_trec "
    #define GREPP30 " |grep \"^P_30\" "
    #define RPREC " |grep \"^Rprec \" "
    #define TEMPFILE1 "/Users/shangsongliang/fileNames.temp"
    #define TEMPFILE2 "/Users/shangsongliang/fileNames2.temp"
    //#define TEMPFILE1 "/scratch/sliang/fileNames.temp"
    //#define TEMPFILE2 "/scratch/sliang/fileNames2.temp"
    #define FORALLFEAEVAFILE "allEvaluation"
    #define CAP "cap"
    #define DENSITY "density"
    #define HASHTAG "hashtag"
    #define ISTOP5 "istop5"
    #define ISTOP10 "istop10"
    #define ISTOP15 "istop15"
    #define ISTOP20 "istop20"
    #define LENGTH "length"
    #define LINK "link"
    #define QUERY "query"
    #define RANKERS "ranker"
    #define RETWEET "retweet"
    //LEARAT: learning rate
    #define LEARNRATE 0.0001
    //momentum parameter
    #define MOMPAR 0.0
}

#endif






