//
//  CombSUM.h
//  Fusion
//
//  Created by Shangsong Liang on 9/17/16.
//  Copyright 2016 __MyCompanyName__. All rights reserved.
//
#ifndef _COMBSUMMNZ_H
#define _COMBSUMMNZ_H

#include "DataTypesTwo.h"
#include "Runs.h"
#include <string>
#include <vector>

namespace lss
{
    class CombSUMMNZ
    {
    public:
        CombSUMMNZ(const lss::Runs& runs);
        lss::QUEDOCSCO_T getQDSSUM();//query-document-score for CombSUM
        lss::QUEDOCSCO_T getQDSMNZ();//query-document-score for CombMNZ
        lss::QUEDOCSCO_T getQTDSSUM();//query-top documents-score of CombSUM
        lss::QUEDOCSCO_T getQTDSMNZ();//query-top documents-score of CombMNZ
    private:
        void wriQDSSUMMNZ(lss::Runs runs);//write to qDSSUM and also write to qDSMNZ
        lss::QUEDOCSCO_T qDSSUM_;//query-document-score(rank) for CombSUM
        lss::QUEDOCSCO_T qDSMNZ_;//query-document-score(rank) for CombMNZ
        lss::QUEDOCSCO_T qTDSSUM_;//query-top documents-score with CombSUM
        lss::QUEDOCSCO_T qTDSMNZ_;//query-top documents-score with CombMNZ
        void wriEvaResult();//write evaluation result
        std::clock_t start_;
        std::clock_t finish_;
    };
}

#endif