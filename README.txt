==Before using this software, you have to do the following steps:

   1. install the Lemur toolkit. You can download the Lemur toolkit from http://sourceforge.net/
      projects/lemur/files/lemur/
   2. install the trec_eval software from the TREC website
   3. copy the "trec_eval" exe file to "fuse/data" after you install the trec_eval software
   4. Open the 'Makefile' file, change the value of "Lemur_dir" to your Lemur instal path, e.g.,
      if you install the Lemur toolkit in /usr/local, you have to change "Lemur_dir" to be
      "Lemur_dir=/usr/local"
   5. cd to the root "fuse" and type "make", you will see there is a exe file called "fuse"
   6. open your terminal and type "./fuse data/fuse.conf". After you press "enter", the software
      will run immediately. The experimental results will save to "result" folder.

==Usege
  The syntax of the program is the following: ./fuse <parameterfile>
  e.g., ./fuse data/fuse.conf 
  
==Parameters in the configure file (data/fuse.conf)
  index: the index of your data set built by Lemur toolkit
  runs: the retrieval lists to be fused
  qrelGT: the ground true file
  trecEval: the trec_eval exe file after you install the trec_eval software
  docNumInRun: number of documents in each retrieval list
  smoothing: smoothing method
  smoothingPar: smoothing parameter for the smoothing method
  topStanScoreDocNum: number of documents used as pseudo label data during fusion
  alphaa: the weight used in manifold fusion
  maxInteration: the maximur times of iteration in manifold fusion
  aveSquErr: average square error used in manifold fusion
  delta: number of documents in each cluster in cluser based fusion
  lambda: the weight used in cluster based fusion
  resultPath: the path which will save the result file
  combSUM: the name of fusion result files using combSUM method
  combMNZ: the name of fusion result files using combMNZ method
  manifoldCombSUM: the name of fusion result files using combSUM score with manifold 
                   based fusion
  manifoldCombMNZ: the name of fusion result files using combMNZ score with manifold 
                   based fusion
  a-manifoldCombSUM: the name of fusion result files using combSUM score with efficient manifold 
                   based fusion
  a-manifoldCombMNZ: the name of fusion result files using combMNZ score with efficient manifold 
                   based fusion
  clusterCombSUM: the name of fusion result files using combSUM score with cluster 
                   based fusion
  clusterCombMNZ: the name of fusion result files using combMNZ score with cluster 
                   based fusion
  a-clusterCombSUM: the name of fusion result files using combSUM score with efficient cluster 
                   based fusion
  a-clusterCombMNZ: the name of fusion result files using combMNZ score with efficient cluster 
                   based fusion
  The following are the corresponding virtual adversarial manifold learning result lists' name:
  v-manifoldCombSUM: the name of fusion result files using combSUM score with manifold 
                   based fusion
  v-manifoldCombMNZ: the name of fusion result files using combMNZ score with manifold 
                   based fusion
  v-a-manifoldCombSUM: the name of fusion result files using combSUM score with efficient manifold 
                   based fusion
  v-a-manifoldCombMNZ: the name of fusion result files using combMNZ score with efficient manifold 
                   based fusion
  v-clusterCombSUM: the name of fusion result files using combSUM score with cluster 
                   based fusion
  v-clusterCombMNZ: the name of fusion result files using combMNZ score with cluster 
                   based fusion
  v-a-clusterCombSUM: the name of fusion result files using combSUM score with efficient cluster 
                   based fusion
  v-a-clusterCombMNZ: the name of fusion result files using combMNZ score with efficient cluster 
                   based fusion
  
==
We keep updating the code these days.
If you have any question, please contact Shangsong Liang by email: shangsong.liang@ucl.ac.uk
__
Copyright (c) 2016, Shangsong Liang, department of computer science, University College London, UK. 
All rights reserved.